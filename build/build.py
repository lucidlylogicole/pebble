import sys, os, json

#---Load Config
with open('.config','r') as f:
    config = json.load(f)
# Load copyright text
with open('../pebble.js','r') as f:
    copyright = f.readline()[3:].strip()


#---Compile SCSS
def compile_scss(filename,new_file=None,style='expanded'):
    import sass
    if new_file == None:
        new_file = os.path.splitext(filename)[0]+'.css'

    css = '/* '+copyright+' */\n'
    css += sass.compile(filename=filename,output_style=style)
    with open(new_file,'w') as f:
        f.write(css)

print('compiling scss')
os.chdir('../scss')
# print(os.path.abspath('.'))
compile_scss('../build/pebble.scss','../pebble.css')
compile_scss('../scss/calendar.scss','../widgets/calendar.css')


#---Combine Widgets
print('combining widgets')
js_copyright = '// '+copyright+'\n'
js_txt = js_copyright
for file in os.listdir('../widgets'):
    if file.endswith('.js'):
        # print(file)
        js_txt += '//---\n//---------------------------------\n'
        with open('../widgets/'+file,'r') as f:
            js_txt += f.read()+'\n'
with open('../dist/pebble-widgets.js','w') as f:
    f.write(js_txt)

#---Combine all js
print('combining js')
with open('../pebble.js','r') as f:
    js_txt = f.read()+'\n'+js_txt
with open('../dist/pebble-all.js','w') as f:
    f.write(js_txt)

#---Minify
print('minifying')
compile_scss('../build/pebble.scss','../dist/pebble.min.css',style="compressed")
compile_scss('../dist/pebble-all.scss','../dist/pebble-all.min.css',style="compressed")
compile_scss('../scss/calendar.scss','../dist/pebble-calendar.min.css',style="compressed")
compile_scss('../scss/icons.scss','../dist/pebble-icons.css')
compile_scss('../scss/icons.scss','../dist/pebble-icons.min.css',style="compressed")
os.system(config['uglifyjs']+' "../pebble.js" -c -o "../dist/pebble.min.js"')
os.system(config['uglifyjs']+' "../dist/pebble-widgets.js" -c -o "../dist/pebble-widgets.min.js"')
os.system(config['uglifyjs']+' "../dist/pebble-all.js" -c -o "../dist/pebble-all.min.js"')

#--- Add Copyright to minified js
for file in ['../dist/pebble.min.js','../dist/pebble-widgets.min.js','../dist/pebble-all.min.js']:
    with open(file,'r') as f:
        js_txt = f.read()+'\n'
    with open(file,'w') as f:
        f.write(js_copyright+js_txt)

#---Update Docs
print('updating docs')
os.chdir('../docs_src')
sys.path.append('.')
import generate_docs
generate_docs.compile(1)

# Update Markdown Docs
import pystache,commonmark
md_docs = [
    {'title':'Form','file':'form.md'},
    {'title':'Spreadsheet','file':'spreadsheet.md'},
    {'title':'Icons','file':'icons.md'},
    {'title':'List','file':'list.md'},
    {'title':'Tree','file':'tree.md'},
]
for mD in md_docs:
    mdfile = mD['file']
    # convert commonmark
    parser = commonmark.Parser()
    with open(mdfile,'r') as f:
        ast = parser.parse(f.read())
        renderer = commonmark.HtmlRenderer()
        mdcontent = renderer.render(ast)
    
    # Get mustache template
    with open('page_template.thtml','r') as f:
        tmpl = f.read()
    u_renderer = pystache.Renderer(string_encoding='utf8')
    html = u_renderer.render(tmpl,{'content':mdcontent,'title':mD['title']})
    
    # Write File
    with open('../docs/'+mdfile.replace('.md','.html'),'w') as f:
        f.write(html)