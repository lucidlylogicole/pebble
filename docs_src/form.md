# [Pebble](docs.html) &rsaquo; Form
Build a form using a JavaScript config and get/set values.

## Demos
- [Form Demo](examples/form.html)

## Install

Include **pebble.js**, **pebble.css**, and **widgets/form.js**

## Quick Start Example

    <div id="form_container"></div>

    <script>
        let fields= [
            {title:'Text Field',type:'text',tooltip:'a text field'},
            {title:'Number Field',type:'number'},
            {title:'Date Field',type:'date'},
            {title:'Combobox Field',type:'combobox',items:['Apple','Grape','Pizza']},
            {title:'Checkbox Field',type:'checkbox'},
        ]
        
        let form = $P.form({parent:'form_container',fields:fields})
        form.getValues()
    </script>

## Documentation

### $P.form(options) - returns a `Form` object
- `options` (dict)
    - `parent` - (string) - id of element to add form fields to
    - `fields` - (array) - array of field objects
    - `layout` - (string) - (undefined, 'column', 'row') the default is undefined/row where the input is next to the label.  the column layout is mode where the label is above the form field (default is 0 which is not wide)
    - `format` (int) - the default format used for the  getValues function (default is 0 or undefined)
    - `viewonly` (bool) - set the form as view only

- **Form** - the main form object (returned from $P.form)
    - **.element** - returns the main element containing the form
    - **.fields** - (array) list of all the field objects
    - **.addField(fieldD)** - add a field to the form
    - **.getElements(field_index)** - return a dictionary of the created elements for the field at field_index.
        - `input` - the input element
        - `label` - the field label/title element
        - `row` - the containing field form row
    - **.clearValues()** - clear all the field values (if defaults are specified, it will set to default)
    - **.setValue(field_index, val)** - set the value of the field at the field index
    - **.setValues(valueD)** - set a bunch of values
        - `valueD` - (dict) - a dictionary of ids to value or field index to value
    - **.getValue(field_index)** - get the value at the field index
    - **.getValues(format)** - get all field values (returns an array or dict)
        - `format` (int) - the return format.
            - `0` (default) an array
            - `1` a dictionary of the id:value
    - **setEnabled(field_index, enabled)** - set a field to enabled or disabled
        - `field_index` (int) - the index of the field
        - `enabled` (bool) - true to enable the field, false to disable it
    - **checkRequired()** - return an array of field_indexes for the blank but required fields
        - an empty array will be returned if all required fields are met
    - **rebuild()** - clear the form elements and rebuild based on changes to `Form.fields`

_**Note:** - the `field_index` is the index for a given field starting from 0 and incrementing in order for all fields.  All fields include heading, hidden, disabled, notes, etc._


### Fields
The field object defines all the field information and settings. Depending on the field type, there may be additional options


**field options** - all fields have the following options and most are optional

- `type` - type of field
- `title` - the label text
- `id` - element id to use
- `tooltip` - hover over tooltip
- `attr` - (dict) - set additional html attributes
- `events` - (dict) - set event listeners (key=event name, value = event function)
- `val` - set the value for the field
- `default` - the default value for the field (will be set if val is not defined or when the form is cleared)
- `display` - optional display settings
    - `'visible'` - show the field (default)
    - `'hidden'` - add the field but hide it
    - `'disabled'` - add the field but the input is disabled
    - `'none'` - don't add the field at all
- `required` (bool) - optionally specify if the field is required (default is false)
    - note: The following fields cannot be required: checkbox, toggleswitch, heading, separator, button
- `placeholder` - optional placeholder text for text and number fields
- `rtext` - optional text to the right of a field given a class of `form-rtext`


**Field Types**

- **button** - add a button
    - `click` - specify the click function
- **checkbox** - checkbox input
- **combobox** - a select field
    - `items` - (array) an array of the select options
        - the item in the array can be a single value or a dictionary specifying the title and value in the format:
            - `{title:'', val:''}`
- **custom** - specify a custom field
    - `element` - provide the custom element to add to the form
- **date** - a date field
- **heading** - a full width heading
- **list** - a select field with options to make it a list
    - `items` - (array) an array of the select options
        - the item in the array can be a single value or a dictionary specifying the title and value in the format:
            - `{title:'', val:''}`
    - `size` - (int) - number of rows for the list
    - `multiple` - (boolean) set to true to allow multiple options to be selected
- **note** - a note or description field that is the full width
    - `create` - specify elements in the create hypertext format
    - `html` - or set the innerHTML
- **number** - a number field
    - `attr` (dict) - to set additional options like the min,max, step, use the attr definition 
        - ex: `{attr:{min:0,max:100}`
- **select** - same as the combobox or list depending on the options
- **separator** - add a separator. this does count as a field in the index, although the value will be undefined
    - `line` - (boolean) display a line for the separator (default is false)
- **text** - a text field
    - `maxlength` - max character length of field
- **textarea** - a larger text box
    - `maxlength` - max character length of field
    - `rows` - the number of visible rows
- **toggleswitch** - similar to a checkbox but with a toggle switch view

**Additional Types**
- if one of the above types is not found then a input element will be created and the type attribute of the element will be set to the 'type' specified

Example Fields:

    let fields = [
        {type:'text',title:'Name', maxlength:100},
        {type:'number',title:'Age', attr:{min:0,step:1,max:130}},
        {type:'combobox', title:'Fruit', items:['Apple','Orange','Peach']}
        {type:'separator'},
        {type:'textarea',title:'address'},
        {type:'date',title:'Join Date'},
    ]

## Form Dialog

### $P.dialog.form(options) - returns a dialog promise
- `options` (dict)
    - any of the form options
    - `size` - optional size for the dialog (undefined, 'wide', 'full', 'none')
    - `title` - title for the dialog
    - `onload(Form)` - an optional function that is called after the form is built that provides the Form object

Example

    let fields= [
        {title:'Text Field',type:'text',tooltip:'a text field'},
        {title:'Number Field',type:'number'},
        {title:'Date Field',type:'date'},
        {title:'Combobox Field',type:'combobox',items:['Apple','Grape','Pizza']},
        {title:'Checkbox Field',type:'checkbox'},
    ]
    
    $P.dialog.form({fields:fields}).then(resp=>{
        console.log(resp)
    })