# [Pebble](docs.html) &rsaquo; List
A list widget with multi selection options

## Demos
- [List Demo](examples/list.html)

## Install

Include **pebble.js**, **pebble.css**, and **widgets/list.js** or **dist/pebble-all.min.js**

## Quick Start Example

    <div id="list_container"></div>

    <script>
        let items= [
            {title:'Option 1',val:1},
            {title:'Option 2',val:2},
            {title:'Option A',val:'a'},
            {title:'Option B',val:'b'},
        ]
        
        let list = $P.list({parent:'list_container',items:items})
    </script>

## Documentation

### $P.list(options) - returns a `List` object
- `options` (dict)
    - `parent` - (string or element) - id of element to add the list element to
    - `multiple` - (bool) - allow multiple selection (default is 0)
    - `editable` - (bool) - allow user to select items in the list

- **List** - the main list object (returned from $P.list)
    - **.element** - returns the main element containing the list
    - **.setItems(items)** - clear the list and set the list items
    - **.setValue(val)** - set the current selection (single value or array of values if multiple is enabled)
    - **.getValue()** - return the selection (single value or array if multiple is enabled)
    - **.clearSelection()** - clear the selection
    - **.selectItem(val)** - set the item with val selected
    - **.click(event)** - the click event when the list is clicked on. override this to customize the action


## List Dialog

### $P.dialog.list(options) - returns a dialog promise
- `options` (dict)
    - any of the list options
    - `size` - optional size for the dialog (undefined, 'wide', 'full', 'none')
    - `title` - title for the dialog
    - `onload(List)` - an optional function that is called after the list dialog is built that provides the List object

### List Dialog Example

    let items= [
        {title:'Option 1',val:1},
        {title:'Option 2',val:2},
        {title:'Option A',val:'a'},
        {title:'Option B',val:'b'},
    ]
    
    $P.dialog.list({title:'Select Option',items:items}).then(resp=>{
        if (resp !== undefined) {
            
        }
    })