# [Pebble](docs.html) &rsaquo; Tree
Tree layout and build functions

## Examples
- [Tree Building](examples/tree.html) - build trees with HTML, JavaScript objects and JavaScript arrays
- [Tree Drag and Drop](examples/tree_drag_and_drop.html)

## Install

Include **pebble.js**, **pebble.css**, and **widgets/tree.js**

## Tree Layout
Pebble uses the following structure for trees. This can be built manually or through the `$P.tree.build` function.

    <div class="tree-container">                <!--Main Tree Element-->

        <div class="tree-block">                <!--A Tree Item Block-->
        
            <div class="tree-item">             <!--The tree item-->
                <div class="tree-btn"></div>    <!--Collapse/Expand Button-->
                <img class="tree-icon">         <!--Optional Icon-->
                <div class="tree-text"></div>   <!--The Text for the tree item-->
            </div>
            
            <div class="tree-childs">           <!--The Item Children (more tree-blocks)-->
                ...
            </div>
            
        </div>

    </div>

## $P.tree API
- **.collapse(elm_or_id)** - collapse a tree item by specifying the `tree-item` element or its children
- **.expand(elm_or_id)** - expand a tree item by specifying the `tree-item` element or its children
- **.toggle(elm_or_id)** - collapse or expand a `tree item` by specifying the tree-item element or its children
- **.collapseAll(elm_or_id)** - collapse all items in a tree by specifying the `tree-container` element
- **.expandAll(elm_or_id)** - expand all items in a tree by specifying the `tree-container` element
- **expandToItem(elm_or_id)** - expand all parents of the element
- **parent(elm_or_id)** - get the parent tree element of the element
- **.getItems(elm_or_id)** - get an array of each tree item and its parent
    - returns [[child_item, parent_item],...]
    - `elm_or_id` - can be the main tree container or a tree item to get the children of a specific branch
- **build(options)** - build a tree from data
    - **options**
        - **items** (array) an array of top level item dictionaries to build
            - item dictionary keys
                - **text** (string) - text for the item
                - **icon** (string) - optionally specify the src to include an icon
                - **items** (array) - specify any children to be added
                - additional keys in the $P.create format will be added to `tree-item`
        - **data** (array) specify the data array to build a tree from
        - _use only `items` or `data` to build a tree, and not both.  See examples below_
        - **parent** (elm_or_id) - specify the parent `tree-container` to add the tree-items to.
        - **itemClick(tree_item_elm)** (function) - optionally specify a click function that will be called when a `tree-item` is clicked (but not if the expand/collapse button is clicked).  The `tree-item` element will be passed to the function
- **.createTreeItem(options)** - a function for building a single tree item (and any children)
    - this function is used by the build function, but could also be used to manually add items to an existing tree
    - returns a `tree-block` element
    - **options** - the options are the same as used for the build items
        - **text** (string) - text for the item
        - **icon** (string) - optionally specify the src to include an icon
        - **items** (array) - specify any children to be added
        - additional keys in the $P.create format will be added to `tree-item`
- **.setDragAndDrop(elm_or_id, options)** - setup drag and drop on a tree
    - **options**
        - **attr** (string) the html attribute returned on drop (default is `tree-id`)
        - **beforeDrop(dropD)** (function) - called before completing a drop
            - `dropD` (dict) - info about the drop
                - `drag_attr` (string) attribute of dragged tree item,
                - `drag_element` (element) the drag element
                - `drop_attr` (string) attribute of tree item dropped on
                - `drop_element` (element) the drop element
                - `drop_parent_attr` (string) attribute of the parent item of the dropped element
                - `drop_position` (string) the position of the drop
                    - `above` - above the drop item (parent is the same as drop item)
                    - `below` - below the drop item (parent is the same as drop item)
                    - `child` - as a child of the drop item
        - **afterDrop(dropD)*** (function) - called after completing a drop
            - `dropD` (dict) - same dictionary as *beforeDrop*

## Building a Tree
There are 2 options for formats that can be used to build a tree.

### 1. Build with a Structured Dictionary (items)
This method is useful when writing the code as the structure mimics the tree

- additional elements can be added to the `tree-item` element by specifying `c` and using the same syntax as `$P.create`
- children are specified with the `items` array
- additional attributes can be included in the definition that are compatible with `$P.create`
    - all keys are added to `tree-item` except `icon,text,items`

Example

    let tree = $P.tree.build({
        items:[
            {text:'Parent',c:[{h:'.sticker bg-blue',text:2}], items:[
                {text:'Sub Item 1',items:[
                    {text:'Sub Sub Item'}
                ]},
                {text:'Sub Item 2',h:'.clr-blue',items:[]},
                {text:'Sub Item 3',items:[
                    {text:'Sub Sub Item 1'},
                    {text:'Sub Sub Item 2'}
                ]},
            ]},
            {text:'Parent 2',items:[]},
        ],
        parent:'tree_bld'
    })

### 2. Build with a Parent/Child Array (data)
This method is more useful for easily building from database data with parent/child keys

- the format of the array is `[item_id, 'text', parent_id]`
- the `item_id` MUST be unique
- use a `parent_id` of `null` or `undefined` for top level tree items
- the `text` string can be replaced with a `$P.create` type object

Example

    let tree = $P.tree.build({
        data:[
            [1,'Parent',null],
            [3,'Sub Item 1',1],
            [4,'Sub Item 2',1],
            [5,'Sub Item 3',2],
            [2,'Parent 2',null],
            [55,{h:'#reditem.clr-red',text:'Parent 3','data-id':55},null],
            [6,{h:'a.pebble',href:'https://gitlab.com',     
                text:'Gitlab','data-id':22},55],
        ],
        parent:'tree_bld',
        itemClick:function(elm) {
            // Optional itemClick function
            console.log(elm)
        }
    })

