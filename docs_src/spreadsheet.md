# [Pebble](docs.html) &rsaquo; Spreadsheet
A keyboard friendly table widget with optional editing, sorting, and pagination.

## Demos
- [General Table](examples/spreadsheet.html)
- [Table with Pagination](examples/spreadsheet_pagination.html)
- [Database Table](examples/spreadsheet_db_table.html) - Example integration with a database

## Features
- optionally editable
- multiple column types (text,number,select,date,custom)
- custom columns with edit function specified
- single column sort (optional)
- pagination (optional)
- advanced keyboard cursor movement and editing
    - a lot of effort has been spent for an intuitive and nice keyboard editing experience, although slightly different than typical spreadsheets
- fixed/sticky row and column headers

## Install

Include **pebble.js**, **pebble.css**, and **widgets/spreadsheet.js**

----
## Quick Start Examples

### Create a Simple Table

**HTML** - create a container for the table

    <div id="table_container"></div>

**JS**

    let tbl = $P.spreadsheet({
        parent:'table_container',
        columns:['Column 1','Column B','Column 3rd'],
        data:[[1,2,3],[4,5,6]],
    })

### Create a Table using more options
**HTML** - create a container for the table

    <div id="table_container"></div>

**JS**

    let tbl = $P.spreadsheet({
        parent:'table_container',
        tableId:'newtable', // give an optional id for styling or reference
        columns:[
            {title:'Var X'},
            {title:'Twice X',type:'number'},
            {title:'X Squared',editable:0},
            {title:'Select',type:'select',items:['Option 1','Option B']},
            {title:'Date',type:'date'},
            {title:'Custom',type:'custom',editCell:function(row_index,col_index,val){}},
        ],
        data:[[1,2,3,'','',''],[4,5,6,'Option 1','2021-02-12','']],
        editable:1,   // table is editable
        keyboard:{editRows:1},  // Enable new row, duplicate row, delete row
        sort:1,       // enable sorting
        pages:{enabled:1,size:20}  // use pagination
    })

----
## API
The spreadsheet uses an id for columns and rows which may not correspond to the displayed row number or column number.

### **$P.spreadsheet(options)**
- `options` - (object) all options are optional
    - `parent` - (string) - the id of the container element to add the table to
    - `tableId` - (string) - specify the element id to add when creating the table
    - `columns` - (array) - array of the columns
        - the items of the columns array can be a dictionary or a string
        - _column dict_ - if the column item is a dict, the following keys are available
            - `title` - (string) - the display text for the column
            - `type` - (string) - the type of column ('text', 'number', 'date', 'select', 'custom') the default is 'text'
            - `editable` - (boolean) - if the column is editable (default is true if the table is editable)
            - `editCell` - (function) if the `type=='custom'`, then you can specify the editCell function
            - `items` - (array) - if the `type=='select'`, then use items to specify the options for the combobox.
            - `dbName` (string) - optionally store a database column name with the column
    - `data` - the data for the table.  multiple data formats are available:
        - tab delimited string
        - 2D array
        - for additional options use the `setData` function instead
    - `doc` - (dict) - if using the internal 'doc' structure (defined below), this can be provided instead of data
    - `editable` - (boolean) - is the table editable
    - `sort` - (boolean) - is sorting enabled
    - `keyboard` - keyboard options
        - `mode` - (string) - mode for how key navigation works
            - `'default'` - the default mode
            - `'excel'` - arrow keys and enter work like Excel/LibreOffice
        - `editRows` (boolean) enable the row editing keyboard shortuts for new row, duplicate row, and delete row. (default is false)
    - `pages` - (dict) - pagination options. by default, pagination is not enabled
        - `enabled` - (boolean) - enable pagination
        - `size` - (int) - row size for the pages


### Row Functions
- **.insertRow(options)** - insert a row
    - `options.afterRow` insert after row id `afterRow`
        - set afterRow to 0 to create row at the beginning
    - `options.data` - optionally specify data to insert either as an array or an object of column ids to value `{cid:val}`
- **.deleteRow(row_index)** - delete the row
- **.duplicateRow(row_index)** - duplicate the row or if `row_index===undefined`, then duplicate the current selected row
- **.pasteRow(event)** - paste the copied row onto selected row
- **.rowCount()** - the number of rows
- **.getRowId(row_index)** - get the row id given the row index (hidden rows are included)
- **.currentRow()** - get the index of the current selected row
- **.setRowVisible(row_index, visible)** - set a row to be visible (true) or hidden (false)

### Column Functions
- **.addColumn(options)** (string or dict) add a column
    - `options` - if a string then that will be the title, otherwise the same column dictionary options can be utilized
        - `title` - (string) - the display text for the column
        - `type` - (string) - the type of column ('text', 'number', 'date', 'select', 'custom') the default is 'text'
        - `editable` - (boolean) - if the column is editable (default is true if the table is editable)
        - `editCell` - (function) if the `type=='custom'`, then you can specify the editCell function
        - `items` - (array) - if the `type=='select'`, then use items to specify the options for the combobox.
        - `dbName` (string) - optionally store a database column name with the column
        - `className` (string) - optionally set the className of the cell (td), header (th) and editor (input or select)
        - `align` (string) - optionally set the text alignment
        - `width` (string) - optionally set the width of the column (any valid CSS width)
- **.deleteColumn(col_index)** - delete the column and data
- **.columnCount()** - the number of columns
- **.setColumnOptions(col_index,options)** - change column details like title, editable, visibility
    - `col_index` (int) - the column index
    - `options` (dict) - 1 or more options to update
        - `title` (string) - update the column title
        - `editable` (bool) - set the editability of the column
        - `visible` (bool) - set the visibility of the column
        - `items` (list) - update the options for a combobox
    - Example: `tbl.setColumnOptions(1, {title:'New Title'})`
- **.setColumnVisible(col_index, visible)** - show or hide the column
    - `col_index` (int) - the column index
    - `visible` (boolean) - set the column visible if true or hide if false
- **.sortColumn(col_index, descending, sort_function)** - sort the column
    - `col_index` (int) - the column index
    - `descending` (boolean) - optionally specify whether to sort descending. leaving this as undefined will toggle the sort
    - `sort_function` - optionally provide a function to sort by
- **.getColumnId(col_index)** - get the column id given the column index (hidden columns are included)
- **.getColumnInfo(col_index)** - get the column metadata
    - Example: `tbl.getColumnInfo(0)`
- **.getColumnEditor(col_index)** - get the editor widget tied to the column
- **.currentColumn()** - get the current column index

### Cell Functions
- **.cellVal(row_index,col_index)** - get the value of the cell
- **.setCell(row_index,col_index, value)** - set the value of the cell
- **.cellElement(row_index,col_index)** - get the html td element
- **.selectCell(row_index,col_index)** - select the cell
- **.getCellIds(row_index,col_index)** - get the internal row and column ids (rid,cid) by specifying the current view row and column index (starting at 0)

### View / Pagination Functions
If pagination is enabled, the following functions are available
- **.nextPage()** - goto the next page
- **.prevPage()** - goto the previous page
- **.pageCount()** - the number of pages
- **.gotoPage(page)** - goto a specific page (starting at 1)
- **.clearView(clear_header)** - clear the table and optionally the header
- **.loadView()** - reload the view with the internal data. this function is not really needed and is called by setData, setDoc, etc.

### Data Functions
- **.setData(data,delim)** - set the table data (clears existing data)
    - `data` - can be a string or 2D array.
    - `delim` - optionally use if passing data as a string. The default delimiter is a tab `\t`
- **.getData()** - get the table data in a simple format
    - `{columns:['col 1','col b'], data:[[1,2],[3,4]]}`
- **.getText(options)** - get the table data as a tab delimited string
    - `options` (dict) - customize text output
        - `delim` - optionally specify the column delimiter (default is `\t`)
        - `header` - include column header (default is `true`)

### Doc Functions
$P.spreadsheet has an internal data structure referenced here as a `doc`. This can be stored and loaded instead of the other data formats.
- **.getDoc(get_json)** - get the internal document
    - `get_json` (boolean) - get the format as json (default is false)
- **.setDoc(doc)** - set the internal document and load the view
    - `doc` (string or object) - can specify a json string or an object

### Internal Data
The following are internal data structures utilized by this widget. Some of the parameters may be useful to read.

**_Do not manually modify these values, as it may not get picked up correctly_**

- **Table.selected** - stores the currently selected row and column ids
- **Table.d** - the main internal data structure storage (returned by `Table.getDoc()`)
- **Table.d.columns** - dictionary with the column info (stored by column id)
- **Table.d.data** - stores the cell data in the format `data[row_id][col_id]`
- **Table.elements** - stores the table elements
    - **Table.elements.col_editors[cid]** - each column has one input field that is moved to the current cell.  This element could be modified after creation.
    - **Table.elements.table** - the main table element

### Events
- **cellChanged** - event dispatched when a cell value is changed
    - details = `{rid:rid,cid:cid,val:val,row_index:row_index, col_index:col_index}`
- **cellSelected** - event dispatched when a cell is selected
    - details = `{rid:rid,cid:cid}`
- **cellDblClick** - event dispatched when a cell is double clicked on
    - details = `{rid:rid,cid:cid,row_index:row_index, col_index:col_index}`
- **Table.addEventListener(event_type, event_function, options)** - add an event listener to the table
    - `event_type` (string) - name of the event
    - `event_function` (function) - function to run
    - `options` - optional. Same as the JavaScript addEventListener options.

### Undocumented Functions
- Additional functions not listed here are considered undocumented
- Undocumented functions could change in the future, while the documented API will attempt to be stable.

----
## Keyboard Shortcuts

### Navigation
- `Arrow Up` - move cell up
- `Arrow Down` - move cell down
- `Arrow Left` - move cell left
- `Arrow Right` - move cell right
- `Home` - move to first row
- `End` - move to last row

### Editing
- `a-Z0-9` - a character key will start editing the cell
- `Enter` - start editing the cell or finish editing the cell (except in Excel mode)
- `Ctrl + e` - edit cell
- `Ctrl + c` - copy cell (also used for copy row)
- `Ctrl + v` - paste cell
- `Ctrl + x` - cut cell
- `Alt + Arrow Down` - while editing a select/combobox or date field, this will
show the drop down
- `Delete` - delete the cell value
- `Escape` - stop editing and don't store changes in the cell

#### Row Editing
To modify rows, the `Table.keyboard.editRows` option must be set to true
- `Ctrl + Shift + V` - paste row
- `Alt + n` - insert row after current cell
- `Alt + Shift + N` - insert row before current cell
- `Ctrl + Delete` or `Alt + Delete` - delete the row
- `Ctrl + d` or `Alt + d` - duplicate the row

