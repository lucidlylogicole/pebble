<link href="../dist/pebble-icons.css" rel="stylesheet">

# [Pebble](docs.html) &rsaquo; Icons
Some basic icons are available in `pebble-icons.css` and `pebble-all.css`.

## Usage

    <i class="icon-file"></i>

<i class="icon-blank"></i> **.icon-blank** a blank icon  
<i class="icon-file"></i> **.icon-file**  
<i class="icon-folder"></i> **.icon-folder**  
<i class="icon-save"></i> **.icon-save**  
<i class="icon-download"></i> **.icon-download**  
<i class="icon-edit"></i> **.icon-edit**  
<i class="icon-menu"></i> **.icon-menu**  
<i class="icon-plus"></i> **.icon-plus**  
<i class="icon-minus"></i> **.icon-minus**  
<i class="icon-checkmark"></i> **.icon-checkmark**  
<i class="icon-cross"></i> **.icon-cross**  
<i class="icon-copy"></i> **.icon-copy**  
<i class="icon-paste"></i> **.icon-paste**  
<i class="icon-search"></i> **.icon-search**  
<i class="icon-trash"></i> **.icon-trash**  
<i class="icon-collapse"></i> **.icon-collapse**  
<i class="icon-expand"></i> **.icon-expand**  
<i class="icon-chevron-left"></i> **.icon-chevron-left**  
<i class="icon-chevron-right"></i> **.icon-chevron-right**  
<i class="icon-chevron-up"></i> **.icon-chevron-up**  
<i class="icon-chevron-down"></i> **.icon-chevron-down**  

## White Icons

**.icon-w** - add this class to the icon or parent element to have white icons

<div class="icon-w bg-blue pad">
<i class="icon-blank"></i>
<i class="icon-file"></i>
<i class="icon-folder"></i>
<i class="icon-save"></i>
<i class="icon-download"></i>
<i class="icon-edit"></i> 
<i class="icon-menu"></i>
<i class="icon-plus"></i>
<i class="icon-minus"></i>
<i class="icon-checkmark"></i>
<i class="icon-cross"></i> 
<i class="icon-copy"></i>
<i class="icon-paste"></i>
<i class="icon-search"></i>
<i class="icon-trash"></i>
<i class="icon-chevron-left"></i> 
<i class="icon-chevron-right"></i>
<i class="icon-chevron-up"></i>
<i class="icon-chevron-down"></i>
</div>

## Larger Icons
<style>
    .icon-lg i {
        width:32px !important;
        height:32px !important;
        background-size: 32px 32px;
    }
    .icon-md i {
        width:24px !important;
        height:24px !important;
        background-size: 24px 24px;
    }
</style>
<div class="icon-lg">
<i class="icon-blank"></i>
<i class="icon-file"></i>
<i class="icon-folder"></i>
<i class="icon-save"></i>
<i class="icon-download"></i>
<i class="icon-edit"></i> 
<i class="icon-menu"></i>
<i class="icon-plus"></i>
<i class="icon-minus"></i>
<i class="icon-checkmark"></i>
<i class="icon-cross"></i> 
<i class="icon-copy"></i>
<i class="icon-paste"></i>
<i class="icon-search"></i>
<i class="icon-trash"></i>
<i class="icon-chevron-left"></i> 
<i class="icon-chevron-right"></i>
<i class="icon-chevron-up"></i>
<i class="icon-chevron-down"></i>
</div>
<div class="icon-md">
<i class="icon-blank"></i>
<i class="icon-file"></i>
<i class="icon-folder"></i>
<i class="icon-save"></i>
<i class="icon-download"></i>
<i class="icon-edit"></i> 
<i class="icon-menu"></i>
<i class="icon-plus"></i>
<i class="icon-minus"></i>
<i class="icon-checkmark"></i>
<i class="icon-cross"></i> 
<i class="icon-copy"></i>
<i class="icon-paste"></i>
<i class="icon-search"></i>
<i class="icon-trash"></i>
<i class="icon-chevron-left"></i> 
<i class="icon-chevron-right"></i>
<i class="icon-chevron-up"></i>
<i class="icon-chevron-down"></i>
</div>