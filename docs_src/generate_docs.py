import doc_parser.doc_parser as doc_parser
import codecs, pystache

def compile(verbose=0):
    docs = 'overview,inputs,containers,ui_elements,dialogs,styling,colors,javascript,more'
    
    cnt = 0
    docD = {'title':'Pebble Documentation','pages':[],'tableOfContents':[]}
    
    for d in docs.split(','):
        cnt += 1
        if not verbose: print(d)
        
        document = doc_parser.parse(file=d+'.cht')
        docD['pages'].append({'pg_id':cnt,'sections':[],'title':document['title']})
        tocD = {'title':document['title'],'sub_items':[],'pg_id':cnt}
    
        for section in document['sections']:
            cnt += 1
            if not verbose: print(cnt,section['title'])
            secD = {'title':section['title'],'content':[],'sec_id':cnt}
            tocD['sub_items'].append({'title':section['title'],'sec_id':cnt})
            for content in section['content']:
                
                contD = content
                contD['typ_'+content['typ']]=1
                if  content['typ'] in ['md','html']:
                    contD['raw']=1
                    contD['raw_class']='cls_'+content['typ']
                secD['content'].append(contD)
            docD['pages'][-1]['sections'].append(secD)
        
        docD['tableOfContents'].append(tocD)
        
        if not verbose: print(len(docD['pages'][-1]['sections']))
        
    # Generate Html
    txt = codecs.open('doc_parser/docs_template.thtml','r','utf-8').read()
    u_renderer = pystache.Renderer(string_encoding='utf8')
    h = u_renderer.render(txt,docD)
    
    with open('../docs/docs.html','w') as outfile:
        outfile.write(h)
    

# if markdown, just parse as markdown
# if html, just parse as html??
# add table of contents

if __name__ == '__main__':
    compile()