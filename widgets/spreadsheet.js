try {$P} catch(e) {$P={}}

//---o:
$P.spreadsheet = function(options) {

//---o:
let Table = {
    version:'2.1.2',
    elements:{col_editors:{}}, // Store Elements
    selected:{},
    stack:[],
    sort: {
        enabled:0,
        column:undefined,
        descending:0,
    },
    pages:{ // pagination
        enabled:0,
        size:20,
        current:0,
    },
    editable:0,
    visible_rows:[],
    // mode:'default',
    keyboard: {
        mode:'default',
        editRows:0,
    },
    
    newDoc: function() {
        // Central Data
        //---d
        return {
            v:'0.4',
            counter:{row:100,col:100},
            data:{},
            columns:{},
            view:{rows:[],cols:[]},
            hidden_rows:[],
        }
    },
    
    getDoc: function(get_json) {
        let doc = JSON.stringify(Table.d)
        if (get_json){return doc}
        return JSON.parse(doc)
    },

    setDoc: function(doc) {
        // Clear
        if (Table.elements.container) {
            while (Table.elements.container.firstChild) {Table.elements.container.removeChild(Table.elements.container.firstChild)}
        }
        
        if (typeof(doc) == 'string') {
            // json
            doc = JSON.parse(doc)
        }
        
        Table.d = Table.newDoc()
        for (let ky in doc) {
            if (ky != 'v') {  //ignore version
                Table.d[ky] = doc[ky]
            }
        }
        Table.updateVisibleRows()
        Table.build(options)
        Table.loadView()
    },

    build:function() {
        // Setup Main Containing Element
        Table.element = document.createElement('div')
        Table.element.className = 'table-container flex-col flex'
        
        // Add to container if provided
        if (options.parent) {
            if (typeof(options.parent) == 'string') {
                Table.elements.container = document.getElementById(options.parent)
            } else {
                Table.elements.container = options.parent
            }
            Table.elements.container.appendChild(Table.element)
        }
        
        // Table wrapping div
        Table.elements.table_div = document.createElement('div')
        Table.elements.table_div.style = "flex:1;overflow-y:auto;position:relative;overflow-x:auto"
        Table.element.appendChild(Table.elements.table_div)
        // Table Element
        Table.elements.table = document.createElement('table')
        if (options.tableId) {
            Table.elements.table.id = options.tableId
        }
        Table.elements.table.className = Table.elements.table.className+' spreadsheet'
        Table.elements.table.setAttribute('tabindex',1)
        Table.elements.table_div.appendChild(Table.elements.table)
        // Setup Table Elements
        Table.elements.thead = document.createElement('thead')
        Table.elements.table.appendChild(Table.elements.thead)
        Table.elements.col_header = document.createElement('tr')
        Table.elements.thead.appendChild(Table.elements.col_header)
        Table.elements.cell_corner = document.createElement('th')
        Table.elements.cell_corner.className='cell-corner'
        Table.elements.col_header.appendChild(Table.elements.cell_corner)
        Table.elements.tbody = document.createElement('tbody')
        Table.elements.table.appendChild(Table.elements.tbody)
        
        // Add Pagination
        if (Table.pages.enabled) {
            Table.elements.pages = document.createElement('div')
            Table.element.appendChild(Table.elements.pages)
            Table.elements.pages.className = 'pagination-container'

            // Previous Button
            let pg_p = document.createElement('button')
            pg_p.innerHTML='&lsaquo;'
            pg_p.title = 'previous page'
            pg_p.className = 'btn-sm'
            pg_p.addEventListener('click',Table.prevPage)
            Table.elements.pages.appendChild(pg_p)
            // Next Button
            let pg_n = document.createElement('button')
            pg_n.innerHTML='&rsaquo;'
            pg_n.title = 'next page'
            pg_n.className = 'btn-sm'
            pg_n.addEventListener('click',Table.nextPage)
            Table.elements.pages.appendChild(pg_n)
            
            // Input with Current Page
            Table.elements.page_input = document.createElement('input')
            Table.elements.page_input.setAttribute('type','number')
            Table.elements.page_input.className = 'pebble pagination-i-page'
            Table.elements.page_input.addEventListener('change',function(){Table.loadView(parseInt(Table.elements.page_input.value)-1)})
            Table.elements.pages.appendChild(Table.elements.page_input)
            
            // Total Page Count
            Table.elements.page_total = document.createElement('div')
            Table.elements.page_total.className = 'pagination-count'
            Table.elements.pages.appendChild(Table.elements.page_total)
            
            // First Page Button
            let pg_f = document.createElement('button')
            pg_f.innerHTML='&lsaquo;&lsaquo;'
            pg_f.title = 'first page'
            pg_f.className = 'btn-sm'
            pg_f.addEventListener('click',function(){Table.loadView(0)})
            Table.elements.pages.appendChild(pg_f)
            // Last Page Button
            let pg_l = document.createElement('button')
            pg_l.innerHTML='&rsaquo;&rsaquo;'
            pg_l.title = 'last page'
            pg_l.className = 'btn-sm'
            pg_l.addEventListener('click',function(){Table.loadView(Table.pageCount())})
            Table.elements.pages.appendChild(pg_l)
            
            // On Wheel 
            Table.elements.table.addEventListener('wheel',Table.mouseWheel)
            
        }
        
        // Build Columns
        for (let colD of options.columns) {
            Table.buildColumn(colD)
        }

        // Event Listeners
        Table.elements.table.addEventListener('click',Table.click)
        Table.elements.table.addEventListener('dblclick',Table.dblclick)
        Table.setupKeyEvents()
    },

    setData: function(data,delim) {
        if (delim === undefined) {delim='\t'}
        // Figure out Data Type
        let dtyp = typeof(data)
        if (dtyp == 'string') {
            //csv
            let csvdata = []
            for (let ln of data.split('\n')) {
                let rw = []
                for (let t of ln.split(delim)) {
                    rw.push(t)
                }
                csvdata.push(rw)
            }
            data = csvdata
        }
        
        // Parse Array
        Table.d.data = {}
        Table.d.view.rows = []
        for (let rw of data) {
            let rid = Table.d.counter.row++
            Table.d.view.rows.push(rid)
            Table.d.data[rid]={}
    
            // add cell data
            for (let c=0; c < Table.columnCount(); c++) {
                let cid = Table.d.view.cols[c]
                let col_typ = Table.d.columns[cid].type
                // Get Cell Value
                let val = null
                if (rw.length > c) {
                    val = rw[c]
                }
                
                if (val !== null && val !== '' && val !== undefined) {
                    if (col_typ == 'number') {
                        val = parseFloat(val)
                    } else {
                        val = String(val)
                    }
                    Table.d.data[rid][cid]={v:val}
                }
            }
        }
        
        // Reload View
        Table.updateVisibleRows()
        Table.loadView()

    },

    clear: function() {
        Table.d = Table.newDoc()
        Table.visible_rows=[]
        Table.clearView(true)
        Table.loadView()
        Table.copyRow = undefined
        Table.copyCol = undefined
        Table.copyVal = undefined
    },
    
    clearView: function(clear_header) {
        if (clear_header) {
            while (Table.elements.thead.firstChild) {Table.elements.thead.removeChild(Table.elements.thead.firstChild)}
        }
        
        // Clear table data
        while (Table.elements.tbody.firstChild) {Table.elements.tbody.removeChild(Table.elements.tbody.firstChild)}
    },

    //---
    loadView: function(page) {

        // Clear
        Table.clearView()
        
        if (Table.pages.enabled) {
            
            // Check if page is in range
            if (page !== undefined) {
                if (page < 0) {
                    page = 0
                } else if (page > Table.pageCount()-1) {
                    page = Table.pageCount()-1
                }
            }
            
            // Pagination View
            if (page === undefined) {
                page = Table.pages.current
            } else {
                Table.pages.current = page
            }
            let start = page*Table.pages.size
            let end = start+Table.pages.size
            if (end > Table.visible_rows.length) {
                end = Table.visible_rows.length
            }
            Table.elements.page_input.value = Table.pages.current+1
            
            Table.setPageTotal()
            
            // Build Rows
            for (let r=start; r<end; r++) {
                let rid = Table.visible_rows[r]
                Table.buildRow(rid,Table.d.view.rows.indexOf(rid))
            }
        } else {
            // Normal Table View

            // Build Rows
            for (let r=0; r<Table.d.view.rows.length; r++) {
                let rid = Table.d.view.rows[r]
                Table.buildRow(rid,r)
            }
            // Set Hidden Rows
            if (Table.d.hidden_rows.length) {
                Table._setRowsVisible(Table.d.hidden_rows,0)
            }
        }
    },
    
    //---
    //---Rows
    buildRow: function(rid,row_count,insert_row) {
        // Add the row with rid (row id) to the table
        let rw = []
        
        // Add Row Element
        let row_elm = document.createElement('tr')
        row_elm.setAttribute('data-rid',rid)
        let row_header_cell = document.createElement('th')
        row_header_cell.className='tbl-row-index'
        row_header_cell.innerText=row_count+1
        row_elm.appendChild(row_header_cell)
        if (insert_row) {
            Table.elements.tbody.insertBefore(row_elm,Table.elements.tbody.childNodes[row_count])
        } else {
            Table.elements.tbody.appendChild(row_elm)
        }
    
        for (let c=0; c< Table.d.view.cols.length; c++) {
            let cid = Table.d.view.cols[c]
            
            // Check if column visible
            if (Table.d.columns[cid].visible){
                
                // Create Cell Element
                let cell_elm = document.createElement('td')
                
                // Get Cell Value
                let val = null
                if (Table.d.data[rid][cid] !== undefined) {
                    val = Table.d.data[rid][cid].v
    
                    // Cell Class and style if specified
                    if (Table.d.data[rid][cid].c) {
                        cell_elm.classList.add(Table.d.data[rid][cid].c)
                    }
                    if (Table.d.data[rid][cid].s) {
                        cell_elm.style = Table.d.data[rid][cid].s
                    }
                    
                }
                if (val === null) {val = ''}
                
                // Add Cell Elm
                cell_elm.innerText = val
                if (Table.d.columns[cid].type == 'hyperlink') {
                    cell_elm.className='td_url'
                    // Hyperlink
                    let cell_url = ''
                    if (Table.d.data[rid][cid] !==undefined) {
                        cell_elm.innerText = Table.d.data[rid][cid].v
                        if (Table.d.data[rid][cid].u) {
                            cell_url = Table.d.data[rid][cid].u
                        }
                    }
                    $P.attr(cell_elm,'title',cell_url)
                }
                
                // Add Attributes
                cell_elm.setAttribute('data-cid',cid)
                if (Table.d.columns[cid].align) {
                    cell_elm.style['text-align'] = Table.d.columns[cid].align
                }
                if (Table.d.columns[cid].className) {
                    cell_elm.className = Table.d.columns[cid].className
                }
                row_elm.appendChild(cell_elm)
            }
        }
    },
    
    currentRow: function() {
        return Table.getRowIndex(Table.selected.row)
    },
    
    insertRow: function(options) {
        if (options === undefined) {options = {}}
        
        // Add a new row, optionally before row id
        let rid = Table.d.counter.row++
        Table.d.data[rid]={}
        let row_count = 0
        if (options.afterRow === undefined) {
            // End
            Table.d.view.rows.push(rid)
            row_count = Table.d.view.rows.length-1
        // } else if (options.afterRow == 0) {
        //     // Beginning
        //     options.afterRow = 0
        //     Table.d.view.rows.unshift(rid)
        } else {
            // let cur_ind = Table.d.view.rows.indexOf(options.afterRow)
            // let cur_ind = Table.d.view.rows.indexOf(Table.getRowIndex(options.afterRow))
            let cur_ind = options.afterRow
            row_count = cur_ind+1
            Table.d.view.rows.splice(cur_ind+1,0,rid)
            // let v_ind = Table.visible_rows.indexOf(Table.getRowIndex(options.afterRow))
            // let v_ind = options.afterRow
            // Table.visible_rows.splice(v_ind+1,0,rid)
        }
        // Add data if specified
        if (options.data !== undefined) {
            if (Array.isArray(options.data)) {
                // Add Array
                for (let i in options.data) {
                    let cid = Table.d.view.cols[i]
                    Table.d.data[rid][cid]={v:options.data[i]}
                }
            } else {
                // Add Object
                for (cid in options.data) {
                    // check for object
                    if (typeof(options.data[cid]) == 'object') {
                        Table.d.data[rid][cid]=options.data[cid]
                    } else {
                        Table.d.data[rid][cid]={v:options.data[cid]}
                    }
                }
            }
        }

        Table.updateVisibleRows()
        let cur_col = Table.d.view.cols[0]
        if (Table.selected.col) {
            cur_col = Table.selected.col
            // cur_col = Table.getColumnIndex(Table.selected.col)
        }
        let row_index = Table.getRowIndex(rid)
        if (Table.pages.enabled) {
            Table.buildRow(rid,row_count,true)
            Table.loadView()
            Table._selectCell(rid,cur_col)
        } else {
            
            // Add to table
            Table.buildRow(rid,row_count,true)
            Table._selectCell(rid,cur_col)
            
            // Recount if inserted before
            if (options.afterRow !== undefined && row_count < Table.d.view.rows.length-1) {
                Table.indexRows()
            }
        }
        
        return row_index
    },
    
    deleteRow: function(row_index) {
        
        // Close Cell Editor
        Table.closeCellEdit()
        
        // Get row_id if exists
        if (row_index === undefined) {
            row_index = Table.currentRow()
        }
        let rid = Table.getRowId(row_index)
        if (rid === undefined) {
            console.error('No row id was found to delete for row_index: '+row_index)
            return
        }
        
        // Remove Data
        if (rid in Table.d.data) {
            delete Table.d.data[rid]
        }
        // Remove from view
        let cur_ind = Table.visible_rows.indexOf(rid)
        let cur_i_ind = Table.d.view.rows.indexOf(rid)
        Table.d.view.rows.splice(cur_i_ind,1)
        
        // Update Visible Rows
        Table.updateVisibleRows()
        
        // Remove from Table
        if (Table.pages.enabled) {
            Table.loadView()
        } else {
            Table.elements.tbody.removeChild(Table.elements.tbody.querySelector(`tr[data-rid="${rid}"]`))
            Table.indexRows()
        }
        
        // Select next cell
        let cur_col = Table.selected.col
        let cur_row = 0
        if (cur_ind >= Table.visible_rows.length) {
            cur_row = Table.visible_rows[Table.visible_rows.length-1]
        } else {
            cur_row = Table.visible_rows[cur_ind]
        }

        if (cur_row !== undefined) {
            Table._selectCell(cur_row,cur_col)
        }
    },
    
    duplicateRow: function(copy_row_index) {
        let copy_rid
        if (copy_row_index == undefined) {
            copy_rid = Table.selected.row
            copy_row_index = Table.getRowIndex(Table.selected.row)
        } else {
            copy_rid = Table.getRowIndex(copy_row_index)
        }
        // Duplicate Row
        if (copy_rid !== undefined) {
            // Get Duplicate Data
            let data = {}
            for (let cid in Table.d.data[copy_rid]) {
                data[cid] = {}
                for (let ky in Table.d.data[copy_rid][cid]) {
                    data[cid][ky] = Table.d.data[copy_rid][cid][ky]
                }
            }
            // let before_rid = Table.d.view.rows[Table.d.view.rows.indexOf(copy_rid)+1]
            return Table.insertRow({afterRow:copy_row_index,data:data})
        }
    },
    
    pasteRow: function(event) {
        if (Table.copyRow !== undefined ) {
            for (let cid in Table.d.data[Table.copyRow]) {
                // Table.d.data[Table.copyRow][cid].val
                Table._setCell(Table.selected.row,cid,Table.d.data[Table.copyRow][cid].v)
            }
        }
    },
    
    indexRows: function() {
        // Re-index the row labels
        let i = 0
        for (let row_elm of Table.elements.tbody.childNodes) {
            i++
            row_elm.childNodes[0].innerText=i
        }
    },
    
    rowCount:function(){return Table.d.view.rows.length},
    
    getRowId:function(row_index) {return Table.d.view.rows[row_index]},
    
    getRowIndex:function(row_id) {return Table.d.view.rows.indexOf(row_id)},
     
    updateVisibleRows: function() {
        Table.visible_rows = Table.d.view.rows.filter(v=>!Table.d.hidden_rows.includes(v))
        return Table.visible_rows
    },
    
    showAllRows: function() {
        Table._setRowsVisible(Table.d.hidden_rows,1)
    },
    
    setRowVisible: function(row_index,visible) {
        Table.setRowsVisible([row_index],visible)
    },
    
    setRowsVisible: function(row_indexes,visible) {
        let vrows = []
        for (let r of row_indexes) {
            vrows.push(Table.getRowId(r))
        }
        Table._setRowsVisible(vrows,visible)
    },
    
    _setRowsVisible: function(row_ids,visible) {
        // Update hidden and visible row arrays
        if (visible) {
            for (let rid of row_ids) {
                let cur_ind = Table.d.hidden_rows.indexOf(rid)
                Table.d.hidden_rows.splice(cur_ind,1)
            }
        } else {
            for (let rid of row_ids) {
                if (Table.d.hidden_rows.indexOf(rid) == -1) {
                    Table.d.hidden_rows.push(rid)
                }
            }
        }
        Table.updateVisibleRows()
        
        if (Table.pages.enabled) {
            // If Pagination, reload view
            Table.loadView()
        } else {
            // Hide Rows
            let row_elms = []
            for (let rid of row_ids) {
                row_elms.push(Table.elements.tbody.querySelector(`tr[data-rid="${rid}"]`))
            }
            $P.toggle(row_elms,visible)
        }
        
    },

    //---
    //---Columns
    buildColumn: function(colD) {
        // Add Column Info
        if (typeof(colD) == 'string') {
            colD = {title:colD}
        }
        
        // Add Column struct
        let cid = Table.d.counter.col++
        Table.d.columns[cid] = {
            title:colD.title,
            type:'text',
            // sort_toggle:1,
            editable:Table.editable,
            visible:1,
        }

        // Add Column Element
        let col_elm = document.createElement('th')
        col_elm.innerText = colD.title
        col_elm.setAttribute('data-cid',cid)
        if (colD.align) {col_elm.style['text-align'] = colD.align}
        if (colD.width) {col_elm.style['width'] = colD.width}
        if (colD.className) {col_elm.className = colD.className}

        // Add cid to list and element to header
        if (colD.before_cid === undefined) {
            // End
            Table.d.view.cols.push(cid)
            Table.elements.col_header.appendChild(col_elm)
        } else if (colD.before_cid == 0) {
            // Beginning
            Table.d.view.cols.unshift(cid)
            Table.elements.col_header.insertBefore(col_elm, Table.element.querySelector(`th[data-cid="${Table.d.view.cols[0]}"]`))
        } else {
            // Insert
            let cur_ind = Table.d.view.cols.indexOf(colD.before_cid)
            Table.d.view.cols.splice(cur_ind,0,cid)
            Table.elements.col_header.insertBefore(col_elm, Table.element.querySelector(`th[data-cid="${colD.before_cid}"]`))
        }
        
        // Add extra column attributes
        for (let col_key of ['editable','type','options','editCell','min','max','step','visible','dbName','align','className']) {
            if (col_key in colD) {
                Table.d.columns[cid][col_key] = colD[col_key]
            }
        }
        
        let col_typ = Table.d.columns[cid].type
        if (col_typ == 'combobox'){col_typ = 'select'}
        
        // Editable elements
        let inp_elm
        if (col_typ == 'select') {
            // Select
            inp_elm = document.createElement('select')
            // Add Items
            for (let opt of colD.items) {
                let opt_elm = document.createElement('option')
                opt_elm.value = opt
                opt_elm.text = opt
                inp_elm.appendChild(opt_elm)
            }

        } else {
            // Input
            inp_elm = document.createElement('input')
            if (col_typ == 'text' || col_typ == 'hyperlink') {
                inp_elm.setAttribute('type','text')
                inp_elm.style['text-align']='center'
            } else if (col_typ == 'number') {
                inp_elm.setAttribute('type','number')
                inp_elm.style['text-align']='center'
            } else if (col_typ == 'date') {
                inp_elm.setAttribute('type','date')
            }
            
            // Set alignment if specified
            if (colD.align) {inp_elm.style['text-align'] = colD.align}
            
        }
        inp_elm.style.position = 'absolute'
        inp_elm.style.display = 'none'
        // Table.elements.table.append(inp_elm)
        inp_elm.style['box-sizing']='border-box'
        if (colD.className) {inp_elm.className = colD.className}
        inp_elm.change_ok = 1
        
        //---Input Keypress Event
        inp_elm.addEventListener('keydown', function(event) {
            inp_elm.change_ok=0
            let kyc = event.key
            event.stopPropagation()
            if (kyc == "Enter") {
                // Enter
                inp_elm.change_ok=1
                event.preventDefault()
                Table.closeCellEdit()
                if (Table.keyboard.mode == 'excel') {Table.moveCellSelection(1,0)}
            } else if (kyc == "ArrowDown" && event.altKey) {
                // Down + Alt Key enable cell edit
                inp_elm.change_ok=1
            } else if (kyc == "Escape") {
                // Esc
                Table.closeCellEdit(1)
            } else if (Table.keyboard.mode == 'excel' && col_typ != 'date') {
                if (kyc == "ArrowRight") {
                    Table.closeCellEdit()
                    Table.moveCellSelection(0,1)
                } else if (kyc == "ArrowLeft") {
                    Table.closeCellEdit()
                    Table.moveCellSelection(0,-1)
                } else if (kyc == "ArrowDown" && col_typ != 'select') {
                    event.stopPropagation()
                    Table.closeCellEdit()
                    Table.moveCellSelection(1,0)
                } else if (kyc == "ArrowUp" && col_typ != 'select') {
                    event.stopPropagation()
                    Table.closeCellEdit()
                    Table.moveCellSelection(-1,0)
                } else if (kyc == "Tab" && event.shiftKey) {
                    event.preventDefault()
                    Table.closeCellEdit()
                    Table.moveCellSelection(0,-1)
                } else if (kyc == "Tab") {
                    event.preventDefault()
                    Table.closeCellEdit()
                    Table.moveCellSelection(0,1)
                }
            } else if (kyc == "ArrowDown" && col_typ=='number') {
                event.preventDefault()
            } else if (kyc == "ArrowUp" && col_typ=='number') {
                event.preventDefault()
            }
            
        },{bubbles:false})
        // Keyup Event
        inp_elm.addEventListener('keyup', function(event) {
            inp_elm.change_ok=1
        },{bubbles:false})

        // On change event
        inp_elm.addEventListener('change', function(event) {
            if (inp_elm.change_ok) {
                Table.closeCellEdit()
            }
        })

        Table.elements.col_editors[cid]=inp_elm
        
        // Check Visibility
        if (!Table.d.columns[cid].visible) {
            Table.setColumnVisibility(cid,0)
        }
        
        return cid
        
    },

    currentColumn: function() {
        return Table.getColumnIndex(Table.selected.col)
    },

    addColumn: function(options) {
        if (options === undefined) {options = {}}
        else if (typeof(options) == 'string') {
            options = {title:options}
        }

        let cid = Table.buildColumn(options)
        
        // Reload view
        Table.loadView()

    },
    
    deleteColumn: function(col_index) {
        let cid = Table.getColumnId(col_index)
        // Delete column header
        let col_th = Table.element.querySelector(`th[data-cid="${cid}"]`)
        col_th.parentElement.removeChild(col_th)
        
        // Delete all Cells
        let cell_elms = Table.element.querySelectorAll(`td[data-cid="${cid}"]`)
        for (let elm of cell_elms) {
            elm.parentElement.removeChild(elm)
        }
        
        // Delete from column settings
        Table.d.view.cols.splice(Table.d.view.cols.indexOf(cid),1)
        
        // Delete from data
        for (let rid in Table.d.data) {
            let rwD = Table.d.data[rid]
            if (cid in rwD) { delete rwD[cid] }
        }
        
    },
    
    setColumnOptions: function(col_index,options) {
        let cid = Table.getColumnId(col_index)
        for (let ky in options) {
            if (ky == 'visible') {
                Table.setColumnVisible(col_index,options[ky])
            } else if (ky == 'editable') {
                Table.d.columns[cid][ky] = options[ky]
            } else if (ky == 'title') {
                Table.d.columns[cid][ky] = options[ky]
                $P.text($P.query1(Table.elements.table,`[data-cid="${cid}"]`),options[ky])
            } else if (ky == 'items') {
                let inp_elm = Table.elements.col_editors[cid]
                $P.clear(inp_elm)
                for (let opt of options.items) {
                    let opt_elm = document.createElement('option')
                    opt_elm.value = opt
                    opt_elm.text = opt
                    inp_elm.appendChild(opt_elm)
                }
            }
        }
    },
    
    setColumnVisible: function(col_index,visible) {
        let cid = Table.getColumnId(col_index)
        Table.d.columns[cid].visible = visible
        
        let cell_elms = Table.element.querySelectorAll(`[data-cid="${cid}"]`)
        for (let elm of cell_elms) {
            if (visible) {
                elm.style.display='table-cell'
            } else {
                elm.style.display='none'
            }
        }

    },
    
    columnCount:function() {return Table.d.view.cols.length},
    
    getColumnId:function(col_index){return Table.d.view.cols[col_index]},
    
    getColumnIndex:function(col_id) {return Table.d.view.cols.indexOf(col_id)},
    
    getColumnInfo:function(col_index) {return Table.d.columns[Table.getColumnId(col_index)]},
    
    getColumnEditor: function(col_index) {
        return Table.elements.col_editors[Table.getColumnId(col_index)]
    },
    
    isEditable: function(col_index) {
        // Is Table or Column Editable
        let editable = Table.editable
        if (editable && col_index!==undefined) {
            editable = Table.d.columns[Table.getColumnId(col_index)].editable
        }
        return editable
    },
    
    getVisibleColumns: function() {
        let vis_cols = []
        for (cid of Table.d.view.cols) {
            if (Table.d.columns[cid].visible) {
                vis_cols.push(cid)
            }
        }
        return vis_cols
    },
    
    //---
    //---Events
    addEventListener: function(event_name,event_function,options) {
        // Add event listener to the table
        Table.elements.table.addEventListener(event_name,event_function,options)
    }, 
    
    click: function(event) {
        let elm = event.target
        // Remove Current Selection
        if (elm.tagName == 'TD') {
            let cid = parseInt(elm.getAttribute('data-cid'))
            let rid = parseInt(elm.parentElement.getAttribute('data-rid'))
            Table._selectCell(rid,cid)
            
            if (event.ctrlKey) {
                // Check and Open Hyperlink
                if (Table.d.columns[cid].type == 'hyperlink') {
                    if (Table.d.data[rid][cid] !==undefined && Table.d.data[rid][cid].u) {
                        window.open(Table.d.data[rid][cid].u,'_blank')
                    }
                }
            }
        }
    },
    
    dblclick: function(event) {
        let elm = event.target
        let cid = parseInt(elm.getAttribute('data-cid'))
        let rid = parseInt(elm.parentElement.getAttribute('data-rid'))
        let row_index = Table.getRowIndex(rid)
        let col_index = Table.getColumnIndex(cid)
        if (elm.tagName == 'TD') {
            // Cell
            Table.editCell()
            // Double Click event
            var evt = new CustomEvent('cellDblClick',
                {detail:{rid:rid,cid:cid,row_index:row_index,col_index:col_index}})
            Table.elements.table.dispatchEvent(evt)
        } else if (elm.tagName == 'TH' && elm.getAttribute('data-cid')) {
            // Column Header
            if (Table.sort.enabled) {
                Table.sortColumn(col_index)
            }
        }
    },

    mouseWheel: function(event) {
        if (!event.shiftKey) {
            event.stopPropagation()
            if (event.deltaY > 0) {
                Table.nextPage()
            } else {
                Table.prevPage()
            }
        }
    },

    setupKeyEvents: function() {
        keymap = $P.keymap(Table.elements.table)
        Table.keymap = keymap
        // Navigation
        // Ctrl + Down (move to end)
        keymap.add('End',()=>{Table._selectCell(Table.d.view.rows.slice(-1)[0],Table.selected.col)})
        keymap.add('ArrowDown',()=>{Table._selectCell(Table.d.view.rows.slice(-1)[0],Table.selected.col)},{ctrl:true})
        // Ctrl + Up (move to start)
        keymap.add('Home',()=>{Table._selectCell(Table.d.view.rows[0],Table.selected.col)})
        keymap.add('ArrowUp',()=>{Table._selectCell(Table.d.view.rows[0],Table.selected.col)},{ctrl:true})
        keymap.add('ArrowUp',()=>{Table.moveCellSelection(-1,0)}) // Up
        keymap.add('ArrowDown',()=>{Table.moveCellSelection(1,0)}) // Down
        keymap.add('ArrowLeft',()=>{Table.moveCellSelection(0,-1)})
        keymap.add('ArrowRight',()=>{Table.moveCellSelection(0,1)})
        
        if (Table.pages.enabled) {
            keymap.add('PageUp',()=>{Table.prevPage()})
            keymap.add('PageDown',()=>{Table.nextPage()})
        }

        if (Table.keyboard.mode == 'excel') {
            keymap.add('Tab',()=>{Table.moveCellSelection(0,1)})
            keymap.add('Tab',()=>{Table.moveCellSelection(0,-1)},{shift:true})
        }
        
        // Input
        keymap.add('Enter',()=>{
            if (Table.keyboard.mode == 'excel') {
                Table.moveCellSelection(1,0)
            } else {
                Table.editCell()
            }
        })
        keymap.add('e',()=>{Table.editCell()},{ctrl:true})
        keymap.add('k',()=>{Table.editCellHyperlink(Table.getRowIndex(Table.selected.row),Table.getColumnIndex(Table.selected.col))},{ctrl:true})
        keymap.add('c',()=>{Table.copyCell(event)},{ctrl:true})
        keymap.add('v',()=>{Table.pasteCell(event)},{ctrl:true})
        keymap.add('V',()=>{
            if (Table.keyboard.editRows && Table.isEditable()) {
                Table.pasteRow(event)
            }
        },{shift:true,ctrl:true})
        //     // Ctrl+V (Paste)
        //     if (Table.copyVal !==undefined && Table.isEditable(Table.selected.col)) {
        //         Table.setCell(Table.selected.row,Table.selected.col,Table.copyVal)
        //     }
        // },{ctrl:true})
        keymap.add('x',()=>{
            // Ctrl+X (Cut)
            if (Table.isEditable(Table.getColumnIndex(Table.selected.col))) {
                Table.copyVal=Table.d.data[Table.selected.row][Table.selected.col].v
                Table._setCell(Table.selected.row,Table.selected.col,'')
            }
        },{ctrl:true})

        // Editable Table
        keymap.add('Delete',()=>{
            // Delete
            if (Table.isEditable(Table.getColumnIndex(Table.selected.col))) {
                // Delete cell
                Table._setCell(Table.selected.row,Table.selected.col,'')
            }
        })
        keymap.add('Delete',()=>{
            // Delete Row
            if (Table.keyboard.editRows && Table.isEditable()) {
                Table.deleteRow(Table.currentRow())
            }
        },{alt:true})
        keymap.add('Delete',()=>{
            // Delete Row
            if (Table.keyboard.editRows && Table.isEditable()) {
                Table.deleteRow(Table.currentRow())
            }
        },{ctrl:true})
        
        keymap.add('N',()=>{
            if (Table.keyboard.editRows && Table.isEditable()) {
                // Alt+N - New Row (before)
                let after_rid = 0
                if (Table.selected.row !== undefined) {
                    // after_rid = Table.visible_rows[Table.visible_rows.indexOf(Table.selected.row)-1]
                    after_rid = Table.currentRow()
                }
                if (after_rid === undefined) {after_rid = 0}
                Table.insertRow({afterRow:after_rid})
            }
        },{alt:true,shift:true})
        keymap.add('n',()=>{
            if (Table.keyboard.editRows && Table.isEditable()) {
                // Alt+N - New Row (after)
                Table.insertRow({afterRow:Table.currentRow()})
            }
        },{alt:true})
        keymap.add('d',()=>{
            if (Table.keyboard.editRows && Table.isEditable()) {
                Table.duplicateRow()
            }
        },{ctrl:true}) // Duplicate Row
        keymap.add('d',()=>{
            if (Table.keyboard.editRows && Table.isEditable()) {
                Table.duplicateRow()
            }
        },{alt:true}) // Duplicate Row

        // Else Input Key
        keymap.add('else',()=>{
            if (!event.ctrlKey && !event.altKey) {
                let kyc = event.key
                if (kyc.length < 2) {
                    Table.editCell(Table.getRowIndex(Table.selected.row),Table.getColumnIndex(Table.selected.col),kyc)
                }
            }
        })
        
    },

    //---
    //---Cell
    cellElement: function(row_index,col_index) {
        let rid = Table.getRowId(row_index)
        let cid = Table.getColumnId(col_index)
        return Table._cellElement(rid,cid)
    },
    
    _cellElement: function(rid,cid) {
        return Table.elements.table.querySelector(`tr[data-rid="${rid}"] td[data-cid="${cid}"]`)
    },
    
    cellVal: function(row_index,col_index) {
        let rid = Table.getRowId(row_index)
        let cid = Table.getColumnId(col_index)
        let val
        if (rid in Table.d.data && cid in Table.d.data[rid]) {
            val = Table.d.data[rid][cid].v
        }
        return val
    },
    
    setCell: function(row_index,col_index,val) {
        let rid = Table.getRowId(row_index)
        let cid = Table.getColumnId(col_index)
        Table._setCell(rid,cid,val)
    },
    
    _setCell: function(rid,cid,val) {
        // let col_typ = Table.d.columns[Table.selected.col].type
        let col_typ = Table.d.columns[cid].type
        
        // Add column to row if undefined
        if (Table.d.data[rid][cid]==undefined) {
            Table.d.data[rid][cid] = {}
        }
        
        let v = val
        if (val == undefined || val === '' || val == null) {
            // Blank Value
            Table._cellElement(rid,cid).innerHTML=''
            v = null
            // Table.d.data[rid][cid].v=null
        } else {
            if (col_typ == 'number') {
                val = parseFloat(val)
            }
            v=val
            Table._cellElement(rid,cid).innerText=val
        }
        
        let changed = (Table.d.data[rid][cid].v != v)
        Table.d.data[rid][cid].v=v
        
        // Change Event
        if (changed) {
            let row_index = Table.getRowIndex(rid)
            let col_index = Table.getColumnIndex(cid)
            var evt = new CustomEvent('cellChanged',
                {detail:{rid:rid,cid:cid,row_index:row_index,col_index:col_index,val:val}})
            Table.elements.table.dispatchEvent(evt)
        }
    },

    selectCell: function(row_index, col_index) {
        Table._selectCell(Table.getRowId(row_index),Table.getColumnId(col_index))
    },
    
    _selectCell: function(rid,cid) {
        // Check pagination
        if (Table.pages.enabled) {
            let rc = Table.visible_rows.indexOf(rid)
            let pg = Math.floor(rc/Table.pages.size)
            if (pg != Table.pages.current) {
                Table.loadView(pg)
            }
        }
        
        let cell_elm = Table._cellElement(rid,cid)
        Table.selectCellElement(cell_elm)
        
        // Change Event
        var evt = new CustomEvent('cellSelected',{detail:{rid:rid,cid:cid}})
        Table.elements.table.dispatchEvent(evt)
        
    },
    
    getCellIds: function(row_index,col_index) {
        // Get rid and cid of current row and column indexes
        // return [Table.d.view.rows[row_index],Table.d.view.cols[col_index]]
        return [Table.getRowId(row_index),Table.getColumnId(col_index)]
    },
    
    selectCellElement: function(elm) {
        Table.closeCellEdit()
        
        // Select Cell
        Table.elements.table.querySelectorAll('.selected').forEach(sel_elm=>{
            sel_elm.classList.remove('selected')
        })
        elm.classList.add('selected')
        
        // Select Row
        Table.elements.table.querySelectorAll('tr.selected').forEach(sel_elm=>{
            sel_elm.classList.remove('selected')
        })
        elm.closest('tr').classList.add('selected')
        
        Table.selected.row = parseInt(elm.parentElement.getAttribute('data-rid'))
        Table.selected.col = parseInt(elm.getAttribute('data-cid'))
        
        // Scroll
        elm.scrollIntoView({block: "nearest"})
        
        // Fix For Sticky Headers
        // Check if cell above sticky header and correct
        let elm_rect = elm.getBoundingClientRect()
        let corner_rect = Table.elements.cell_corner.getBoundingClientRect()
        if (elm_rect.top < corner_rect.bottom) {
            Table.elements.table.parentElement.scrollBy(0,elm_rect.top - corner_rect.bottom)
        }
        // Check if cell behind row sticky header and correct
        if (elm_rect.left < corner_rect.right) {
            Table.elements.table.parentElement.scrollBy(elm_rect.left - corner_rect.right,0)
        }
    },
    
    moveCellSelection: function(row_inc,col_inc) {
        let rw = -1
        let col = -1
        
        // Check Selected Row
        if (Table.selected.row !== undefined) {
            rw = Table.visible_rows.indexOf(Table.selected.row)
        }
        // Increment row and check in range
        rw = rw+row_inc
        if (rw >= Table.visible_rows.length) {
            rw = Table.visible_rows.length-1
        } else if (rw < 0) {
            rw = 0
        }
        
        // Columns
        let vis_cols = Table.getVisibleColumns()
        
        // Check Selected Column
        if (Table.selected.col !== undefined) {
            col = vis_cols.indexOf(Table.selected.col)
        }
        // Increment column
        col = col + col_inc
        
        // Check Range
        if (col >= vis_cols.length) {
            col = vis_cols.length-1
        } else if (col < 0) {
            col = 0
        }
        
        // Select Cell
        if (rw !=-1 && col!=-1) {
            Table._selectCell(Table.visible_rows[rw],vis_cols[col])
        }
        
    },
    
    //---Cell Edit
    editCell: function(row_index,col_index,val) {
        let rid = Table.getRowId(row_index)
        let cid = Table.getColumnId(col_index)
        if (row_index === undefined) {
            if (Table.selected.row !== undefined) {
                rid = Table.selected.row
                row_index = Table.getRowIndex(Table.selected.row)
            }
        }
        if (col_index === undefined) {
            if (Table.selected.col !== undefined) {
                cid = Table.selected.col
                col_index = Table.getColumnIndex(Table.selected.col)
            }
        }
        if (!Table.isEditable(col_index)) {
            return
        }
        
        // Create
        let cell_elm = Table.cellElement(row_index,col_index)
        if (cell_elm) {
            
            if (val===undefined) {
                val = Table.cellVal(row_index,col_index)
            }
            
            // If custom column column edit function
            if (Table.d.columns[cid].type == 'custom') {
                Table.d.columns[cid].editCell(row_index,col_index,val)
                return
            }
            
            // Get Column Input Field
            let inp_elm = Table.elements.col_editors[cid]
            Table.current_edit_element = inp_elm
            Table.elements.table.append(inp_elm)
            inp_elm.style.left = cell_elm.offsetLeft+'px'
            inp_elm.style.top = cell_elm.offsetTop+'px'
            if (Table.d.columns[cid].type != 'date') {
                inp_elm.style.width = cell_elm.offsetWidth+'px'
            }
            inp_elm.style.height = cell_elm.offsetHeight+'px'
            inp_elm.style.display='block'
            inp_elm.focus()
            if (val === undefined) {
                inp_elm.value = ''
            } else {
                inp_elm.value = val
            }
        }
    },
    
    closeCellEdit: function(esc) {
        if (Table.current_edit_element !== undefined) {
            Table.current_edit_element.style.display='none'
            if (!esc) {
                Table._setCell(Table.selected.row,Table.selected.col,Table.current_edit_element.value)
            }
            Table.current_edit_element = undefined
            Table.elements.table.focus()
        }
    },
    
    copyCell: function(event) {
        let cell_data = Table.d.data[Table.selected.row][Table.selected.col]
        Table.copyCol=Table.selected.col
        Table.copyRow=Table.selected.row
        Table.copyUrl = ''
        if (cell_data) {
            Table.copyVal=Table.d.data[Table.selected.row][Table.selected.col].v
            if (Table.d.data[Table.selected.row][Table.selected.col].u) {
                Table.copyUrl=Table.d.data[Table.selected.row][Table.selected.col].u
            }
        } else {
            Table.copyVal=''
        }
    },
    
    pasteCell: function(event) {
        // Paste (ctrl+v)
        if (Table.editable && Table.copyVal !==undefined && Table.isEditable(Table.getColumnIndex(Table.selected.col))) {
            // Validate Cell
            let col_typ = Table.d.columns[Table.selected.col].type
            let ok = 0
            if (Table.copyVal == '') {
                ok = 1
            } else if (col_typ == 'text' || (col_typ == 'number' && $P.isNumber(Table.copyVal))) {
                ok = 1
            } else if (col_typ == 'select') {
                if (Table.selected.col == Table.copyCol) {
                    ok = 1
                }
            } else if (col_typ == Table.d.columns[Table.copyCol].type) {
                ok = 1
            }
            if (ok) {
                Table._setCell(Table.selected.row,Table.selected.col,Table.copyVal)
                
                // Set URL
                if (col_typ == 'hyperlink') {
                    Table.setCellHyperlink(Table.getRowIndex(Table.selected.row),Table.getColumnIndex(Table.selected.col),Table.copyUrl)
                }
            }
        }
    },
    
    //---Hyperlink
    setCellHyperlink: function(row_index,col_index,url) {
        let rid = Table.getRowId(row_index)
        let cid = Table.getColumnId(col_index)
        if (Table.d.data[rid][cid]===undefined) {
            Table.d.data[rid][cid] = {}
        }
        Table.d.data[rid][cid].u = url
        $P.attr(Table.cellElement(row_index,col_index),'title',url)
    
        // Change Event
        var evt = new CustomEvent('cellChanged',{detail:{rid:rid,cid:cid,url:url}})
        Table.elements.table.dispatchEvent(evt)
        
    },
    
    editCellHyperlink: function(row_index,col_index,url) {
        let rid = Table.getRowId(row_index)
        let cid = Table.getColumnId(col_index)
        if (url === undefined) {
            url = ''
            if (Table.d.data[rid][cid] && Table.d.data[rid][cid].u) {
                url = Table.d.data[rid][cid].u
            }
        }
        let new_url = prompt('Edit the Link/URL',url)
        if (new_url !== null) {
            Table.setCellHyperlink(row_index,col_index,new_url)
        }
    },
    
    //---
    //---Pagination (pages)
    pageCount:function(){return Math.ceil(Table.visible_rows.length/Table.pages.size)},
    setPageTotal: function() {
        Table.elements.page_total.innerText = 'of '+ Table.pageCount()
    },
    
    nextPage: function() {
        Table.loadView(Table.pages.current+1)
    },

    prevPage: function() {
        Table.loadView(Table.pages.current-1)
    },
    
    gotoPage: function(page) {
        loadView(page)
    },
    
    //---
    //---Sorting
    sortColumn: function(col_index, descending, sort_function) {
        let cid = Table.getColumnId(col_index)
        // Check for descending
        if (descending === undefined) {
            if (Table.sort.column == cid) {
                descending = !Table.sort.descending
            } else {
                descending = 0
            }
        }
        Table.sort.column = cid
        Table.sort.descending = descending
        
        // Set up a row array to sort
        let rows_array = []
        for(let r=0; r<Table.d.view.rows.length; r++) {
            let rid = Table.d.view.rows[r]
            let cv = null
            if (Table.d.data[rid][cid]!==undefined){cv = Table.d.data[rid][cid].v}
            rows_array[r]={old_index:rid, val:cv}
            // If sort by number
            if (Table.d.columns[cid].type == 'number') {
                let v = parseFloat(rows_array[r].val)
                if (!isNaN(v)){rows_array[r].val = v}
            }
        }
        
        // Sort the Array
        if (sort_function !== undefined) {
            // User Defined Sort Function
            rows_array.sort(sort_function)
        } else {
            rows_array.sort((a,b)=>{
                return(a.val == b.val ? 0 : (a.val > b.val ? 1 : -1))
            })
        }
        
        // Update Column indicator class
        let elm_classname = 'sort-asc'
        // Check Descending
        if (descending) {
            rows_array.reverse()
            elm_classname = 'sort-desc'
        }
        
        // Re-append everything to the table
        let new_row_order = []
        for(let r=0; r<rows_array.length; r++) {
            let rid = rows_array[r].old_index
            new_row_order.push(rid)
        }
        Table.d.view.rows = new_row_order
        
        // Set Sort Icon
        Table.elements.table.querySelectorAll('th.sort').forEach(sel_elm=>{
            sel_elm.classList.remove('sort')
            sel_elm.classList.remove('sort-asc')
            sel_elm.classList.remove('sort-desc')
        })
        let th_elm = Table.elements.thead.querySelector(`th[data-cid="${cid}"]`)
        th_elm.classList.add('sort')
        th_elm.classList.add(elm_classname)

        // Reload the view
        Table.updateVisibleRows()
        Table.loadView()

    },
    
    //---Exports
    getData: function(options) {
        // Generate Nice Data Format
        
        Table.closeCellEdit()
        
        if (options === undefined) { options = {}}
        let data = {columns:[],data:[]}
        
        // Get Columns
        for (let cid of Table.d.view.cols) {
            data.columns.push(Table.d.columns[cid].title)
        }
        
        let rows
        // Check to include hidden
        if (options.hidden) {
            rows = Table.d.view.rows
        } else {
            rows = Table.visible_rows
        }
        
        // Get Data
        for (let rid of rows) {
            let rw = []
            
            for (let cid of Table.d.view.cols) {
                let v = undefined
                if (cid in Table.d.data[rid]) {
                    v = Table.d.data[rid][cid].v
                }
                if (options.blank && v===undefined) {
                    v = ''
                }
                rw.push(v)
            }
            data.data.push(rw)
        }
        
        return data
    },
    
    getText: function(options) {
        // Delimited Text
        
        // Check args and options
        if (options === undefined){options={}}
        if (options.header === undefined){options.header=true}
        if (options.delim === undefined) {options.delim = '\t'}
        
        // Get data
        let data = Table.getData({blank:1})
        let txt = ''
        if (options.header) {
            txt += data.columns.join(options.delim)
        }
        for (let rw of data.data) {
            txt += '\n'+rw.join(options.delim)
        }
        return txt
    },
    
    
} // end Table

//---
//---Main Loading
Table.d = Table.newDoc()
if (options.editable) {Table.editable=1}
if (options.keyboard !== undefined) {
    for (let oky in options.keyboard) {
        Table.keyboard[oky]=options.keyboard[oky]
    }
}
if (options.pages !== undefined) {
    for (let pky in Table.pages) {
        if (pky in options.pages) {
            Table.pages[pky] = options.pages[pky]
        }
    }
}

if (options.doc) {
    // Load the document
    Table.setDoc(options.doc)
} else {
    // Load other table format
    
    // Build the Table
    Table.build(options)
    
    if (options.data) {
        Table.setData(options.data)
    }

    if (options.sort) {
        Table.sort.enabled=1
        $P.class.add(Table.elements.table,'spreadsheet-sortable')
    }

}


return Table

}