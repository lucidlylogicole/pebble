
//---o:
$P.calendar = function(options) {
    let elm = $P.elm(options.parent)
    let elm_id = elm.id

    // Get Year and Month
    let CURRENT_DATE = new Date()
    let year, month
    if (options !== undefined) {
        year = options.year
        month = options.month
    }
    if (year === undefined) {
        year = CURRENT_DATE.getFullYear()
    }
    if (month === undefined) {
        month = CURRENT_DATE.getMonth()+1
    }
    
    // Create Calendar
    $P.calendar.list[elm_id] = {
        year:year,
        month:month,
        element:elm_id,
        showDayHeader:1,
        dayContent:undefined,
        selected:[],
        selectable:0,
        singleSelect:1,
        generate:function() {
            // Clear
            // let elm = document.getElementById(this.element)
            if(elm != null) {
                while (elm.firstChild) {
                    elm.removeChild(elm.firstChild);
                }
                elm.appendChild(this.createElements())
            }
        },
        
        createElements: function() {
            let year = this.year
            let month = this.month
            let firstDay = new Date(year, month-1, 1)
            let startingDay = firstDay.getDay()
            
            // Month Length
            let monthLength = $P.date.month_day_counts[month-1]
            if (month == 2) { // February only!
                // compensate for leap year
                if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
                    monthLength = 29
                }
            }
            
            // Main Calendar Container
            let cal_elms = {class:'calendar-container',children:[
            ]}
            
            // Year and Month Selector
            let yr_elms = {class:'cal-sel-year hidden',onclick:"$P.hide(event.target)"}
            yr_elms.children = this.createYearSelect(year)
            cal_elms.children.push(yr_elms)
            
            let mo_elms = {class:'cal-sel-month hidden',children:[],onclick:"$P.hide(event.target)"}
            for (let m =1; m <13; m++) {
                let c = ''
                if (m == month) {c=' active'}
                if (m>1 && m%2) {
                }
                mo_elms.children.push({class:'btn cal-month-btn'+c,text:$P.date.months[m-1].substring(0, 3),onclick:"$P.calendar.get('"+this.element+"').selectMonth("+m+")"})
            }
            cal_elms.children.push(mo_elms)
            
            // Calendar Element Builder
            let elms = {tag:'table',class:'calendar-tbl', onclick:"$P.calendar.get('"+this.element+"').dayClick(event)", children:[
                {tag:'tr',class:'cal-title',children:[
                    {tag:'th',class:'cal-next',text:'&lsaquo;', onclick:'$P.calendar.get(\''+this.element+'\').prevMonth()'},
                    {tag:'th',text2:$P.date.months[month-1]+' '+year, colspan:5, children:[
                        {tag:'span',text:$P.date.months[month-1]+' ',onclick:"$P.calendar.get('"+this.element+"').showMonths()"},
                        {tag:'span',text:year,onclick:"$P.calendar.get('"+this.element+"').showYears()"},
                    ]},
                    {tag:'th',class:'cal-next',text:'&rsaquo;', onclick:'$P.calendar.get(\''+this.element+'\').nextMonth()'},
                ]},
            ]}
            
            cal_elms.children.push(elms)
            
            // Day of Week Header
            if (this.showDayHeader) {
                let day_elms = {tag:'tr', class:'cal-day-header',children:[]}
                for (let i=0; i <=6; i++) {
                    day_elms.children.push({tag:'td',text:$P.date.weekdays[i]})
                }
                elms.children.push(day_elms)
            }
            
            // Add Selectable class
            if (this.selectable) {
                elms.class += ' selectable'
            }
            
            let cdate = new Date() 
            let day = 1
            // Fill in the Days
            for (let i=0; i<9; i++) { // weeks
                let wk_elm = {tag:'tr',children:[]}
                for (let j = 0; j <= 6; j++) { //days
                    if (day <= monthLength && (i > 0 || j >= startingDay)) {
                        let cls_today = ''
                        // Check to highlight today
                        if (day == cdate.getDate() && cdate.getMonth()+1 == month && cdate.getFullYear() == year) {
                            cls_today = ' today'
                        }
                        
                        let day_elm = {tag:'td', class:'cal-day'+cls_today, 'day-id':day,text:day}
                        
                        let day_dt = $P.date.date(year,month,day)
                        
                        // Check if containing content
                        if (this.dayContent !== undefined) {
                            day_elm.text=''
                            day_elm.children=[
                                {tag:'div',class:'cal-day-day',text:day},
                                {tag:'div',class:'cal-day-content',text:''},
                            ]
                            
                            // Add Day Content
                            if (day_dt in this.dayContent) {
                                day_elm.children[1].text=this.dayContent[day_dt]
                            }
                        }
                        
                        // Check if Selectable
                        if (this.selectable) {
                            if (this.isSelected(day_dt)) {
                                day_elm.class+=' selected'
                            }
                        }
                        
                        wk_elm.children.push(day_elm)
                        day ++ 
                    } else {
                        wk_elm.children.push({tag:'td',class:'cal-day-blank'})
                    }
                }
                elms.children.push(wk_elm)
                if (day > monthLength) {break}
            }
            // Add Extra row
            // if (elms.children.length < 8) {
            //     elms.children.push({tag:'tr',children:[{tag:'td',colspan:7,text:'&nbsp;'}]})
            // }
            
            return $P.create(cal_elms)
        
        },
        
        isSelected: function(date) {
            return this.selected.indexOf(date) > -1
        },

        //---Month
        nextMonth:function() {
            this.month ++
            if (this.month > 12) {
                this.month = 1
                this.year ++
            }
            this.generate()
        },
        prevMonth:function() {
            this.month --
            if (this.month < 1) {
                this.month = 12
                this.year --
            }
            this.generate()
        },
        selectMonth:function(month) {
            this.month = month
            this.generate()
        },
        showMonths:function() {
            $P.show($P.query(this.element,'.cal-sel-month')[0])
        },
        
        //---Year
        selectYear:function(year) {
            this.year = year
            this.generate()
        },

        showYears:function() {
            $P.show($P.query(this.element,'.cal-sel-year')[0])
        },

        createYearSelect: function(year) {
            let elms = [
                {class:'btn cal-year-btn',text:'&#9650;',onclick:"$P.calendar.get('"+this.element+"').changeYearSelect("+(year-5)+")"}
            ]
            for (let y=year-2; y < year+3; y++) {
                let c = ''
                if (y == this.year) {c=' active'}
                elms.push({class:'btn cal-year-btn'+c,text:y,onclick:"$P.calendar.get('"+this.element+"').selectYear("+y+")"})
            }
            
            elms.push({class:'btn cal-year-btn',text:'&#9660;',onclick:"$P.calendar.get('"+this.element+"').changeYearSelect("+(year+5)+")"})
            return elms
        },
        
        changeYearSelect: function(year) {
            let elms = $P.create(this.createYearSelect(year))
            let yr_elm = $P.query(this.element,'.cal-sel-year')[0]
            $P.clear(yr_elm)
            for (let e=0; e< elms.length; e++) {
                yr_elm.appendChild(elms[e])
            }
        },

        //---Day
        daySelect: function(day) {
            let yr = this.year
            let mo = this.month
            
            // Check Single Select
            if (this.singleSelect) {
                this.selected = []
            }
            this.selected.push($P.date.str(yr,mo,day))
            
        },
        dayDeselect: function(date) {
            let ind = this.selected.indexOf(date)
            if (ind > -1) {
                this.selected.splice(ind,1)
            }
        },
        dayClick: function(event) {
            if (this.selectable) {
                if (event.target.classList.contains('cal-day')) {
                    let sel = 1
                    let day = parseInt(event.target.getAttribute('day-id'))
                    if (event.target.classList.contains('selected')) {
                        sel = 0
                    }
                    
                    // Remove selection if selected
                    if (this.singleSelect) {
                        let day_elms = $P.query(this.element,'.cal-day')
                        for (let i=0; i < day_elms.length; i++) {
                            if ($P.class.has(day_elms[i],'selected')) {
                                $P.class.remove(day_elms[i],'selected')
                            }
                        }
                    }
                    
                    if (sel) {
                        event.target.className += ' selected'
                        $P.calendar.get(this.element).daySelect(day)
                    } else {
                        event.target.className = 'cal-day'
                        this.dayDeselect($P.date.str(this.year,this.month,day))
                    }
                    
                }
            }
        },

    }
    
    if (options!==undefined) {
        for (ky in options) {
            if (ky != 'year' && ky != 'month') {
                $P.calendar.list[elm_id][ky] = options[ky]
            }
        }
    }
    
    $P.calendar.list[elm_id].generate()
    return $P.calendar.list[elm_id]
    // }
}

// Calendar List
//---o:
$P.calendar.list = {}

//---f:
$P.calendar.get = function(elm_id) {
    return $P.calendar.list[elm_id]
}

//---
//---$P.calendar.dialog(options)
$P.dialog.calendar = function(options) {
    if (options === undefined) {
        options = {}
    }
    if (options.title === undefined) {
        options.title = 'Select Date'
    }
    if (options.selectable === undefined) {
        options.selectable = 1
    }
    
    let dlg_doneP = $P.promise()
    let btns = []
    
    // Create Calendar Element
    let elm_id = 'pc-'+$P.uuid(0)
    let dlg_cal_elm = $P.create({id:elm_id,class:'align-c'})

    // Ok 
    btns.push($P.create({tag:'button',class:'btn',text:'ok'}))
    btns[0].onclick = function(){
        let val = $P.calendar.get(elm_id).selected
        if (val.length == 0) {
            val = undefined
        } else if ($P.calendar.get(elm_id).singleSelect) {
            val = val[0]
        }
        dlg_doneP.accept(val)
        $P.dialog.close()
        delete $P.calendar.list[elm_id]
    }
    // Cancel
    btns.push($P.create({tag:'button',class:'btn',text:'cancel'}))
    btns[1].onclick = function(){
        dlg_doneP.accept()
        $P.dialog.close()
        delete $P.calendar.list[elm_id]
    }
    
    // Create Calendar HTML
    // let dlg_cal_elm = $P.create({id:elm_id,class:'align-c margin'})
    options.parent = dlg_cal_elm
    let cal = $P.calendar(options)
    let cal_h = cal.createElements()
    
    options.content = dlg_cal_elm
    options.buttons = btns
    
    $P.dialog.build(options)
    
    return dlg_doneP
}