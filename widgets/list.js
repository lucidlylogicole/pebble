
//---o:
$P.list = function(options) {

//---o:
let List = {
    version:'0.3.1',
    multiple:0,     // select multiple items
    editable:1,     // user can select items
    autoscroll:0,   // auto scroll to selection
    
    build: function() {
        
        
        List.element = $P.create({h:'.li-item-container', c:[]})
        
        // Setup Options
        if (options.multiple !== undefined) {List.multiple=options.multiple}
        if (options.editable !== undefined) {List.editable=options.editable }
        if (options.autoscroll !== undefined) {List.autoscroll=options.autoscroll }
        
        if (List.editable) {
            $P.class.add(List.element,'selectable')
            
            // Setup Click Event
            List.element.onclick = this.click
        }
        
        // Add Items
        if (options.items) {
            List.setItems(options.items)
        }
        
        // Add to parent
        if (options.parent) {
            $P.elm(options.parent).appendChild(List.element)
        }
        
        // Set Value
        if (options.val) {
            List.setValue(options.val)
            
            // Scroll to value
            if (options.autoscroll) {
                List.scrollToSelection()
            }
        }
        
    },

    setItems: function(items) {
        $P.clear(List.element)
        let elms = []
        for (let itm of items) {
            if ($P.isObject(itm)) {
                elms.push({h:'.li-item', text:itm.title, 'data-val':itm.val})
            } else {
                elms.push({h:'.li-item', text:itm, 'data-val':itm})
            }
        }
        
        $P.create(elms, {parent:List.element})
    },
    
    setValue: function(val) {
        if (List.multiple && $P.isArray(val)) {
            List.clearSelection()
            for (let v of val) {
                List.selectItem(v)
            }
        } else {
            List.selectItem(val)
        }
    },
    
    clearSelection: function() {
        $P.class.remove($P.query(List.element,'.li-item.selected'),'selected')
    },
    
    getValue: function() {
        let val
        let sel_items = $P.query(List.element,'.li-item.selected')
        if (List.multiple) {
            val = []
            for (let itm of sel_items) {
                val.push($P.attr(itm,'data-val'))
            }
        } else if (sel_items.length > 0) {
            val = $P.attr(sel_items[0],'data-val')
        }
        return val
    },
    
    selectItem: function(val){
        if (!List.multiple) {List.clearSelection()}
        $P.class.add($P.query1(List.element,`.li-item[data-val="${val}"`),'selected')
    },
    
    click: function(event) {
        // console.log(event)
        if (!List.multiple) {List.clearSelection()}
        
        $P.class.toggle(event.target,'selected')
    },
    
    scrollToSelection: function(force) {
        // Scroll to currently selected item
        let sel_elm = $P.query1(List.element,'.selected')
        if (sel_elm && (force || $P.isVisible(List.element))) {
            sel_elm.scrollIntoView({block:'center'})
        }
    },

}

List.build()

return List
}

//---o:
$P.dialog.list = function(options) {
    options.content = {c:[]}
    if (options.list !== undefined) {options.items = options.list}
    
    if (options.text !== undefined && options.text != '') {
        options.content.c.push({h:'.dlg-content pad-b',text:options.text})
    }
    
    let dlg_doneP = $P.promise()
    
    let list_widget = $P.list(options)
    
    // Add list and setup onlick
    options.content.c.push(list_widget.element)
    
    options.buttons = []
    if (options.multiple) {
        // Add Ok for multiselect
        // Ok
        options.buttons.push($P.create({h:'button.btn',text:'ok'}))
        options.buttons[0].onclick = function(event){
            dlg_doneP.accept(list_widget.getValue())
            $P.dialog.close(event.target)
        }
    } else {
        // Single Click accepts promise
        list_widget.element.onclick = function(event) {
            $P.class.toggle(event.target,'selected')
            dlg_doneP.accept(list_widget.getValue())
            $P.dialog.close(event.target)
        }
    }
    // Cancel
    options.buttons.push($P.create({h:'button.btn',text:'cancel'}))
    options.buttons.slice(-1)[0].onclick = function(event){
        dlg_doneP.accept()
        $P.dialog.close(event.target)
    }

    $P.dialog.build(options)

    // Scroll to value
    if (options.val !== undefined) {
        list_widget.scrollToSelection()
    }

    // Optional Onload call
    if (options.onload !== undefined) {
        options.onload(list_widget)
    }

    return dlg_doneP
}