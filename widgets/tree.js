
//---o:
$P.tree = {
    version:'1.4.0',
    collapse:function(elm_or_id) {
        $P.class.remove($P.elm(elm_or_id).closest('.tree-block'),'expand')
    },

    expand:function(elm_or_id) {
        $P.class.add($P.elm(elm_or_id).closest('.tree-block'),'expand')
    },
    
    expandToItem:function(elm_or_id) {
        function expandParent(elm) {
            let par_elm = $P.tree.parent(elm)
            if (par_elm !== undefined) {
                $P.tree.expand($P.tree.parent(elm))
                expandParent(par_elm)
            }
        }
        expandParent(elm_or_id)
    },
    
    toggle:function(elm_or_id) {
        let tree_itm = $P.elm(elm_or_id).closest('.tree-block')
        if ($P.class.has(tree_itm,'expand')) {
            $P.class.remove(tree_itm,'expand')
        } else {
            $P.class.add(tree_itm,'expand')
        }
    },

    collapseAll: function(elm_or_id) {
        $P.class.remove($P.query(elm_or_id,'.tree-block'),'expand')
    },

    expandAll: function(elm_or_id) {
        $P.class.add($P.query(elm_or_id,'.tree-block'),'expand')
    },

    click: function(event) {
        let elm = event.target
        if ($P.class.has(elm,'tree-btn')) {
            $P.tree.toggle(elm)
        }
    },
    
    parent: function(elm_or_id) {
        let pitm
        let itm = $P.elm(elm_or_id)
        if ($P.class.has(itm.parentElement.parentElement,'tree-item-childs')) {
            pitm = itm.parentElement.parentElement.parentElement.children[0]
        }
        return pitm
    },
    
    getItems: function(elm_or_id) {
        let items = []
        for (let itm of $P.query(elm_or_id,'.tree-item')) {
            let pitm = $P.tree.parent(itm)
            items.push([itm,pitm])
        }
        return items
    },

//---
//---Builder Functions
    createTreeItem: function(options) {
        let itm = options
        // Basic Tree Item
        let itm_elm = {h:'.tree-block', c:[
            {h:'.tree-item', c:[
                {h:'.tree-btn'},
            ]},
            {h:'.tree-item-childs',c:[]},
        ]}
        
        let tr_itm = itm_elm.c[0]
        
        // Grab Icon
        if (itm.icon) {
            tr_itm.c.push({h:'.tree-icon', src:itm.icon})
        }

        // Add Attributes to tree-item
        for (let ky in itm) {
            if (['icon','text','items','c'].indexOf(ky)<0) {
                tr_itm[ky] = itm[ky]
            }
        }
        // Make sure has tree-item class
        if (tr_itm.h.indexOf('tree-item')<0) {
            if (tr_itm.h.indexOf('.')<0) {
                tr_itm.h += '.tree-item'
            } else {
                tr_itm.h = $P.replaceAll(tr_itm.h,'.','.tree-item ')
            }
        }

        // Item Text
        tr_itm.c.push({h:'.tree-text',text:itm.text})

        // Add Children 
        if (itm.c) {
            for (let itm_c of itm.c) {
                tr_itm.c.push(itm_c)
            }
        }
        
        // Children
        if (itm.items && itm.items.length > 0) {
            for (let itm_c of itm.items) {
                itm_elm.c[1].c.push($P.tree.createTreeItem(itm_c))
            }
        } else {
            tr_itm.c[0].h = '.tree-btn none'
        }
        
        return itm_elm
    },
    
    build: function(options) {
        
        let tree_items = []
        
        if (options.items) {
        
        //---Tree Dict Build
            for (let itm of options.items) {
                tree_items.push($P.tree.createTreeItem(itm))
            }
            
        } else if (options.data) {
            
        //---Tree Data Build
            let rD = {}
            // Create items
            for (let row of options.data) {
                let rid = row[0]
                let opt= {text:row[1],'tree-id':rid}
                if (typeof(row[1]) == 'object') {
                    opt = row[1]
                }
                let itm = $P.tree.createTreeItem(opt)
                rD[rid] = itm
            }
            // Add To Parent
            for (let row of options.data) {
                let rid = row[0]
                let pid = row[2] // parent id
                if (pid == null || pid == undefined) {
                    tree_items.push(rD[rid])
                } else if (pid in rD) {
                    rD[pid].c[1].c.push(rD[rid])
                    rD[pid].c[0].c[0].h = '.tree-btn'
                }
            }
        }
        
        // Check for Item Click
        let click_function = $P.tree.click
        if (options.itemClick) {
            click_function = function(event) {
                let elm = event.target
                if ($P.class.has(elm,'tree-btn')) {
                    $P.tree.toggle(elm)
                } else if ($P.class.has(elm.parentElement,'tree-item')) {
                    options.itemClick(elm.parentElement)
                }
            }
        }
        
        // Add tree click event (if not there)
        if (options.parent && !$P.attr(options.parent,'has-click-event')) {
            $P.listen(options.parent,'click',click_function)
            $P.attr(options.parent,'has-click-event',true)
        }
        return $P.create(tree_items,{parent:options.parent})
        
    },
    
//---
//---Drag and Drop
    setDragAndDrop: function(elm_or_id, options) {
        
        let drag_elm
        if (options === undefined){options={}}
        if (options.attr === undefined) {
            options.attr = 'tree-id'
        }
        
        //---f:
        $P.setDropEvent({
            element:$P.elm(elm_or_id),
            drop_pos:undefined,
            onDrop:async function(event,drag_data,drag_type){
                // Clear Classes
                this.clearDropClasses()
                
                let tree_item
                if ($P.class.has(event.target,'tree-item')) {
                    tree_item = event.target
                } else if ($P.class.has(event.target.parentElement,'tree-item')) {
                    tree_item = event.target.parentElement
                }
                
                if (tree_item) {
                    
                    // Check to not move to itself or in itself
                    if (drag_elm == tree_item) {
                        return
                    }
                    else if (drag_elm.parentElement.contains(tree_item)) {
                        alert('Cannot drag an item into itself')
                        return
                    }
                    
                    // get parent before move
                    let prev_drag_parent = drag_elm.parentElement.parentElement.parentElement
                    let new_parent = tree_item.parentElement.parentElement.parentElement
                    if (this.drop_pos == 'child') {
                        new_parent = tree_item.parentElement
                    }
                    let drop_data = $P.attr(tree_item,options.attr)
                    let drop_parent_data = $P.attr($P.query1(new_parent,'.tree-item'),options.attr)
                    // check if top level item
                    if (!$P.class.has(new_parent,'tree-block')) {
                        drop_parent_data = null
                    }
                    
                    // Drop Info
                    let dropD = {
                        drag_attr:drag_data,
                        drag_element:drag_elm,
                        drop_attr:drop_data,
                        drop_element:tree_item,
                        drop_parent_attr:drop_parent_data,
                        drop_position:this.drop_pos,
                    }
                    
                    let ok = 1
                    if (options.beforeDrop !== undefined) {
                        ok = await options.beforeDrop(dropD)
                    }
                    
                    if (ok) {
                        // Move Item
                        if (this.drop_pos == 'child') {
                            new_parent = tree_item.parentElement
                            $P.query1(tree_item.parentElement,'.tree-item-childs').appendChild(drag_elm.parentElement)
                            // add expand/collapse button
                            $P.class.remove($P.query1(tree_item,'.tree-btn'),'none')
                        } else if (this.drop_pos == 'above') {
                            tree_item.parentElement.before(drag_elm.parentElement)
                        } else if (this.drop_pos == 'below') {
                            tree_item.parentElement.after(drag_elm.parentElement)
                        }
                        
                        // reclass parent of drag element has children or not
                        if ($P.class.has(prev_drag_parent,'tree-block') && $P.query1(prev_drag_parent,'.tree-item-childs').children.length == 0) {
                            $P.class.add($P.query1(prev_drag_parent,'.tree-btn'),'none')
                        }
                        
                        if (options.afterDrop !== undefined) {
                            options.afterDrop(dropD)
                        }
                        
                    }
                    
                }
            },
            clearDropClasses: function(tree_item) {
                $P.class.remove($P.query(this.element,'.drag-over-t'),'drag-over-t')
                $P.class.remove($P.query(this.element,'.drag-over-b'),'drag-over-b')
                $P.class.remove($P.query(this.element,'.drag-over-box'),'drag-over-box')
            },
            dragOver:function(ev){
                let tree_item
                
                if ($P.class.has(ev.target,'tree-item')) {
                    tree_item = ev.target
                } else if ($P.class.has(ev.target.parentElement,'tree-item')) {
                    tree_item = ev.target.parentElement
                }
                
                if (tree_item) {
                    let bounding = tree_item.getBoundingClientRect()
                    let offset = event.clientY - bounding.y
                    let partial =  (bounding.height/3)
                    
                    // Clear classes
                    this.clearDropClasses()
                    
                    if ( offset > partial*2 ) { // below
                        $P.class.add(tree_item,'drag-over-b')
                        this.drop_pos = 'below'
                    } else if (offset < partial ) { // above
                        $P.class.add(tree_item,'drag-over-t')
                        this.drop_pos = 'above'
                    } else { // into
                        $P.class.add(tree_item,'drag-over-box')
                        this.drop_pos = 'child'
                    }
                }

            },
            dragLeave:function(ev){
                this.drop_pos=undefined
                this.clearDropClasses()
            },
        })
        
        //---f:
        $P.setDragEvent({
            element:$P.elm(elm_or_id),
            attr:options.attr,
            type:'item',
            dragStart:function(ev) {
                drag_elm = ev.target
                $P.class.add(ev.target,'drag-item')
                $P.class.add(this.element,'dragable-tree')
            },
            dragEnd:function(ev) {
                $P.class.remove(ev.target,'drag-item')
                $P.class.remove(this.element,'dragable-tree')
            },
        })

        //---f:
        $P.setDraggable($P.query($P.elm(elm_or_id),'.tree-item'))
        
        
    },
    
}
