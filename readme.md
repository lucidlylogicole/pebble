# Pebble - JS/CSS Library
Pebble is a web library with modern styles that can be utilized when you want. Pebble has fully styled elements, utility styles and useful JavaScript functions. The goal of Pebble is to improve efficiency on commonly used tasks, while leaving most of the customization to the developer.

Pebble can be used with normal web sites and web apps. Many styled elements do not require JavaScript, while some do (like tabs and page changes). Pebble also includes convenient JavaScript functions like `$P.elm()` in replace of `document.getElementById()`.

---

## Documentation
- **[Documentation](https://lucidlylogicole.gitlab.io/pebble/docs/docs.html)** - view the docs
- **[Example Page](https://lucidlylogicole.gitlab.io/pebble/docs/ui_sample.html)** - View a bunch of styled sample elements

## Installation
1. Download **[pebble.css](https://gitlab.com/lucidlylogicole/pebble/raw/master/pebble.css)** for the styles
2. Download  **[pebble.js](https://gitlab.com/lucidlylogicole/pebble/raw/master/pebble.js)** to utilize the JavaScript functions
3. Include the css and js files
        
        <link href="pebble.css" rel="stylesheet" media="screen">
        <script src="pebble.js"></script>

## Alternate Builds

- [dist/pebble.min.css](https://gitlab.com/lucidlylogicole/pebble/raw/master/dist/pebble.min.css) - minified styles
- [dist/pebble.min.js](https://gitlab.com/lucidlylogicole/pebble/raw/master/dist/pebble.min.js) - minified JavaScript functions
- [dist/pebble-widgets.min.js](https://gitlab.com/lucidlylogicole/pebble/raw/master/dist/pebble-widgets.min.js) - all the JavaScript widgets combined
- [dist/pebble-all.min.js](https://gitlab.com/lucidlylogicole/pebble/raw/master/dist/pebble-all.min.js) - combines `pebble.js` and all the widgets
- [dist/pebble-all.min.css](https://gitlab.com/lucidlylogicole/pebble/raw/master/dist/pebble-all.min.css) - combines `pebble.css` and all the widget styles

## Requirements
- **Nothing** really. Pebble does not depend on jQuery or any other frameworks
- A **"modern" web browser** is likely needed if you want everything to work
    - Chrome and Firefox are fully supported, with attempts to make sure most things work with Safari and Edge.

## Philosophy
- Improve efficiency on commonly used tasks in web interface creation
- All Pebble features are optional (it will not override anything by default)
- Provide a simple, modern looking base style
- Allow for easy customization (don't fight Pebble to customize the interface)
- Simple functional JavaScript
- Provide most useful features instead of trying to do everything
- Minimize breaking backwards compatibility of the API

----

*Note: Pebble is an upgrade and replacement to the Pristine library.  See the [changelog.md](docs/changelog.md) for more info.*